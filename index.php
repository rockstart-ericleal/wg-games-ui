<!DOCTYPE html>
<html>
<head>
	<meta name="theme-color" content="#000000" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	
	<link rel="stylesheet" type="text/css" href="./css/game-style.css">
	<script type="text/javascript" src="https://staging-wg-v2.herokuapp.com/static/js/md5.js"></script>

	<!-- <meta name="url-ad" content="/flyer-ba.png" /> -->
	<!-- <meta name="url-ad" content="/flyer-banamex.jpg" /> -->
	<!-- <meta name="url-ad" content="/flyer-coca-cola.jpg" /> -->
	<!-- <meta name="url-ad" content="/flyer-tp.jpg" /> -->
	<!-- <meta name="url-ad" content="/flyer.marti.jpeg" /> -->
	<!-- <meta name="url-ad" content="/bbva.jpg" />				 -->
	<!-- <meta name="url-ad" content="/epura.jpg" /> -->
	<!-- <meta name="url-ad" content="/flyer-coca-cola.jpg" /> -->

	<meta name="production" content="false" />

	<?php
		$path = "";
		switch($_GET['game']){
			case "jetpack":
				$path = "jetpack";
				?>
				<title>Games | Jetpack</title>
				<meta name="url-ad" content="/bbva.png" />				
				<meta property="og:title" content="Games | Jetpack">
				<meta property="og:image" content="https://game.webfamous.mx/poster-2.png">
				<meta property="og:description" content="WiFi Gratis Demo Games">
				<meta property="og:url" content="https://game.webfamous.mx/?game=jetpack">
				<meta name="twitter:card" content="summary_large_image">
				<?php
			break;
			case "superball":
				$path = "superball";
				?><meta name="url-ad" content="/flyer-coca-cola.jpg" />

				<title>Games | Superball</title>
				<meta property="og:title" content="Games | Superball">
				<meta property="og:image" content="https://game.webfamous.mx/poster-1.png">
				<meta property="og:description" content="WiFi Gratis Demo Games">
				<meta property="og:url" content="https://game.webfamous.mx/?game=superball">
				<meta name="twitter:card" content="summary_large_image">
				<?php
			break;
			case "virus-extremo":
				$path = "moto-extrema";
				?><meta name="url-ad" content="/flyer_del_verde.jpeg" />

				<title>Games | Virus Extremo</title>
				<meta property="og:title" content="Games | Virus Extremo">
				<meta property="og:image" content="https://game.webfamous.mx/poster-3.png">
				<meta property="og:description" content="WiFi Gratis Demo Games">
				<meta property="og:url" content="https://game.webfamous.mx/?game=virus-extremo">
				<meta name="twitter:card" content="summary_large_image">
				<?php
			break;
				?><?php
			default:

		}

	?>

	<meta name="domain-assets" content="http://localhost:8000/static" />
	<meta name="type-flyer" content="image" />

	<meta name="login-url-only" content="http://flyervirtual.connect/login" />
	<meta name="chap-id" content="\003" />
	<meta name="chap-challenge" content="\147\103\164\060\143\206\311\355\050\153\327\267\270\031\022\347" />
	<meta name="dst" content="http://conecta.wifigratis.mx/usuarios/track/1/4/index?lp=aHR0cHM6Ly93aWZpZ3JhdGlzLm14Lw==" />

	<meta name="token" content="eyJhbGciOiJIUzI1NiIsIn" />
	<meta name="best-score" content="0" />
	<meta name="mi-score" content="0" />
	<meta name="user-name" content="Santiago" />
	<meta name="level" content="1" />

	<meta name="anuncio-id" content="1" />
	<meta name="game-name" content="moto-extreme" />
	<meta name="anuncio-game-id" content="1" />

	<meta name="user-id" content="4" />


</head>
<body>
	<form name="sendin" action="http://flyervirtual.connect/login" method="post">
		<input type="hidden" name="username" />
		<input type="hidden" name="password" />
		<input type="hidden" name="dst" value="http://conecta.wifigratis.mx/usuarios/track/1/4/index?lp=aHR0cHM6Ly93aWZpZ3JhdGlzLm14Lw==" />
		<input type="hidden" name="popup" value="false" />
	</form>

	<div id="render-game"></div>
	<script src="./node_modules/phaser/dist/phaser.js"></script>
	<!-- <script type="text/javascript" src="./games/jetpack/dist/main-min.js?v=0.6"></script> -->
	<!-- <script type="text/javascript" src="./games/superball/dist/main-min.js?v=0.6"></script> -->
	<script type="text/javascript" src="./games/<?php echo $path ?>/dist/main-min.js?v=0.52"></script>

</body>
</html>