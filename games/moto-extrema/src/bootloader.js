import gameOptions from './config.js'
import { _$, log, metadata, ratioDisplay } from "../../src/utils.js"

var isLaunch = false

class Bootloader extends Phaser.Scene{
	constructor(){
		super("Bootloader")
	}

	init(){

	}

	preload(){

			//Load Flyer
		this.load.image('flyer', gameOptions.urlAds)	

		this.load.path = gameOptions.assetsPath()
		
		//Bootloader
		this.load.multiatlas('UI','/images/data-images.json')	
		this.load.image('logo','/images/logo-wifi.png')
		

		//Flyer		
		this.load.multiatlas('FLYER','/images/data-images.json')	
		this.load.image('top-fade-black', './images/top-fade-black.png')


		//moto-extrema
		this.load.image('road', '/images/road.png')
		this.load.image('moto', '/images/moto.png?v=0.4')
		this.load.image('bullet', '/images/bullet.png')
		this.load.image('bullet-2', '/images/bullet-2.png')

		this.load.spritesheet('peon-1', '/images/peon-virus-1.png', { frameWidth:200, frameHeight: 200 })
		this.load.spritesheet('peon-2', '/images/peon-virus-2.png', { frameWidth:200, frameHeight: 200 })
		this.load.spritesheet('peon-3', '/images/peon-virus-3.png', { frameWidth:200, frameHeight: 200 })
		this.load.spritesheet('peon-4', '/images/peon-virus-4.png', { frameWidth:200, frameHeight: 200 })

		this.load.image('icon-min-2x', '/images/icon-min-2x_.png')
		this.load.image('icon-heart-2x', '/images/icon-heart-2x.png')
		
		this.load.spritesheet('general-1', '/images/general-1.png', { frameWidth:200, frameHeight: 200 })
		this.load.spritesheet('general-2', '/images/general-2.png', { frameWidth:200, frameHeight: 200 })
		this.load.spritesheet('general-3', '/images/general-3.png', { frameWidth:200, frameHeight: 200 })
		this.load.spritesheet('general-4', '/images/general-4.png', { frameWidth:200, frameHeight: 200 })

		this.load.image('bullet-virus', '/images/bullet-virus.png')

		this.load.spritesheet('boss-1', '/images/boss-1.png', { frameWidth:200, frameHeight: 200 })
		this.load.spritesheet('boss-2', '/images/boss-2.png', { frameWidth:200, frameHeight: 200 })
		this.load.spritesheet('boss-3', '/images/boss-3.png', { frameWidth:200, frameHeight: 200 })
		this.load.spritesheet('boss-4', '/images/boss-4.png', { frameWidth:200, frameHeight: 200 })
		
		this.load.atlas('exploit-0', '/anims/exploit-4.png', '/anims/exploit-4_atlas.json')
		this.load.atlas('exploit-1', '/anims/exploit-3.png', '/anims/exploit-3_atlas.json')
		this.load.atlas('exploit-2', '/anims/exploit-2.png', '/anims/exploit-2_atlas.json')


		this.load.audio('shot-1', ['./sounds/shoot-7.mp3'])
		this.load.audio('shot-2', ['./sounds/shoot-8.mp3'])
		this.load.audio('moto-pain', ['./sounds/pain-2.mp3'])
		this.load.audio('pop-1', ['./sounds/pop-1.mp3'])
		this.load.audio('evil-sound', ['./sounds/evil-sound.mp3'])
		this.load.audio('evil-sound-2', ['./sounds/evil-sound-2.mp3'])
		this.load.audio('winner-1', ['./sounds/winner_1.mp3'])
		this.load.audio('next-level-up', ['./sounds/next-level-up.mp3'])
		this.load.audio('pistol', ['./sounds/pistol.mp3'])


		//UI
		//Load Sounds
		this.load.audio('explosion-shine','./sounds/explosion-shine.mp3')
		this.load.audio('coins','./sounds/coins.mp3')
		this.load.audio('next-level-up','./sounds/next-level-up.mp3')


		//Start Game
		let ratio = ratioDisplay()
		// log('Ratio', ratio)
		this.load.image('start-game-bg-top', `./images/start-game-bg-top@${ratio}.png`)
		this.load.image('start-game-bg-bottom', `./images/start-game-bg-bottom@${ratio}.png`)
		this.load.image('play-yellow', `./images/play-yellow@${ratio}.png`)
		this.load.image('super-ball-header', `./images/moto-top@${ratio}.png`)
		this.load.image('super-ball-bottom', `./images/moto-bottom@${ratio}.png`)
		this.load.image('title-connect', `./images/title-connect@${ratio}.png`)



		
		let rectloader = this.add.graphics();
		rectloader.fillStyle(0xffffff, 1);
		rectloader.fillRoundedRect((this.scale.width/2)-63, this.scale.height/2, 126, 9, 3);

		let textPorcentage = this.make.text({
			x: this.scale.width/2,
            y: this.scale.height/2 - 32,
            text: '0%',
            style: {
                fontSize: '32px', align: 'left', fontFamily: "'Geogrotesque Medium'"
            }
        })

        textPorcentage.setOrigin(0.5)

		let progressBar = this.add.graphics();

		this.load.on('progress', (value) => {
			let porcentage = Math.round((value * 100))
			let progress = (porcentage * 120)/100
			progressBar.clear();
			progressBar.fillStyle(0x00070D, 1);
			progressBar.fillRect((this.scale.width/2) -60, this.scale.height/2 + 3, progress, 3);			
			

			this.add.tween({
				targets:textPorcentage,
				duration:200,
				ease:'Bounce',
				scale:{ from: 1.2, to: 1},
				onComplete: () =>{
					textPorcentage.text = `${porcentage}%`
				}
			})

			if (porcentage >= 100 ){
				
				this.add.tween({
					targets: [rectloader, progressBar, textPorcentage],
					alpha:{from: 1, to: 0},
					duration: 500,
					repeat:0,
					delay:1000,
					onConplete: () => {
						this.time.delayedCall(600, ()=>{
							this.loadLogo()
						})
					}
				})
				// log('porcentage', porcentage)


			}

		})

		
	}

	loadLogo(){
		this.logo = this.add.image(this.scale.width/2, this.scale.height/2, 'logo')
		this.logo.setAlpha(0)
		this.logo.setScale(0.6)

		if(!isLaunch){
			isLaunch = true
			this.time.delayedCall(300, ()=>{

				this.add.tween({
				targets: [this.logo],
				alpha:{from: 0, to: 1},
				duration: 800,
				repeat:0,
				onComplete: () => {

					this.add.tween({

						targets: [this.logo],
						alpha:{from: 1, to: 0},
						duration: 400,
						repeat:0,
						delay:1000,
						onComplete: () =>{
							this.scene.start('StartGame')
							this.scene.start('Flyer')
						}

					})

										
				}
			})

			})

		}	
		


	}



	create(){

		

		// graphics.x = this.camera.width/2
		// graphics.x = this.camera.height/2
		
	}

}

export default Bootloader