import { _$, log, metadata } from "../../src/utils.js"
import gameOptions from './config.js';

import Bootloader from './bootloader.js'
import Flyer from './flyer.js';
import Moto from './moto-extreme.js';
import UI from './UI.js';
import StartGame from './start-game.js'


window.onload = () => {
	let config = {

		title:'WiFi Gratis',
		url: 'https://wifigratis.mx',
		version:'0.0.1',
		banner: {
			hidePhaser: true,
			background:['#001957']
		},
	
		type: Phaser.AUTO,
		backgroundColor: 0x111B24,
		scale: {
			mode: Phaser.Scale.FIT,
			autoCenter: Phaser.Scale.CENTER_BOTH,
			parent: "render-game",
			width: ((window.innerWidth > 500) ? 500 : window.innerWidth),
			height: window.innerHeight,	
		},
		activePointers: 1,
		physics: {
			default: "arcade",
			arcade:{
				// gravity: {
				// 	y:800
				// },
				// debug: true,
			}
		},
		scene: [Bootloader, Flyer, Moto, UI, StartGame]
	}

	const game = new Phaser.Game(config);
	window.focus();


	window.onerror = (errorMsg, url, lineNumber) => {
		console.log(errorMsg,url,lineNumber);
	    let el = document.querySelector("body");
		el.innerHTML = '<h5>'+errorMsg+'</h5><a href="'+url+'">'+url+'</a><h5>'+lineNumber+'</h5>';
	};
}