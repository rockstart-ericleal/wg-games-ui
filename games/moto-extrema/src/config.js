import { _$, log, metadata } from "../../src/utils.js"

const gameOptions = {
	birdGravity: 0,
	birdSpeed: 125,
	birdFlapPower: 300,
	minPipeHeight: 200,
	pipeDistance:[ 300, 380],
	pipeHole: [70, 100],
	localStorage: 'localStorage',
	localStorageName: "score-wg",
	amount_stars: 16,
	urlAds: metadata()["url-ad"],
	chap: metadata()["chap-id"],
	chapChallenge: metadata()["chap-challenge"],
	dst: metadata()["dst"],
	
	miScore: metadata()["mi-score"],
	userName: metadata()["user-name"].toUpperCase(),	
	domainAssets : metadata()["domain-assets"],
	
	token:metadata()["token"],
	bestScore: parseInt(metadata()["best-score"]),
	anuncioId: parseInt(metadata()['anuncio-id']),	
	level: parseInt(metadata()["level"]),
	gameName: metadata()["game-name"],
	userId: parseInt(metadata()['user-id']),
	anuncioGameId: parseInt(metadata()['anuncio-game-id']),

	typeFlyer: metadata()['type-flyer'],
	totalEnemy: 4,
	margin: 100,
	initLife: 4,

	production: metadata()['production'],
	assetsPath: function(){
		if( this.production === 'true'){
			return this.domainAssets + '/games/moto-extrema/assets/'
		}else{
			return './games/moto-extrema/assets/'
		}
	}


};

export default gameOptions;