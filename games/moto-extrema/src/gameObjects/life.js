import gameOptions from '../config.js'
class Life extends Phaser.Physics.Arcade.Group{
	constructor(config){
		super(config.physicsWorld, config.scene)		
		this.scene = config.scene
		this.texure = 'icon-heart-2x'
		this.level = config.level || 1			
	}

	addLife(x, y){
		let life = this.create(x, y, this.texure)		
		life.setScale(0.5)		
		life.setAlpha(1)
		life.catch = false		
		life.setVelocityY(100)
		life.y = Phaser.Math.Between(-700, -900)

		this.scene.add.tween({
			targets:life,
			scale: {from: 0.6, to: 0.50},			
			duration:800,
			yoyo:true,
			repeat:-1,
		})
		
	}

	allDestroy(){		
		this.children.iterate( (life) =>{
			life.destroy()
		})
	}


	distributelife(){
		this.children.iterate( (life) =>{
			if(!life.catch){
				if(life.y > this.scene.scale.height + 32){
					life.y = Phaser.Math.Between(-700, -900)
					life.x = Phaser.Math.Between( gameOptions.margin, this.scene.scale.width - gameOptions.margin)
				}
			}
		})

		if(this.getChildren().length <= 0){
			this.addLife( Phaser.Math.Between( gameOptions.margin, this.scene.scale.width - gameOptions.margin) , -32)					
		}

		console.log(this.children)
	}

}

export default Life
