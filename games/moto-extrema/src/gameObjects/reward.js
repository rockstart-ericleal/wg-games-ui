import gameOptions from '../config.js'
class Reward extends Phaser.Physics.Arcade.Group{
	constructor(config){
		super(config.physicsWorld, config.scene)		
		this.scene = config.scene
		this.texure = 'icon-min-2x'
		this.level = config.level || 1			
	}

	addReward(x, y){
		let reward = this.create(x, y, this.texure)		
		reward.setScale(0.35)		
		reward.setAlpha(1)
		reward.catch = false		
		reward.setVelocityY(100)
		reward.y = Phaser.Math.Between(-700, -900)

		this.scene.add.tween({
			targets:reward,			
			props:{
				angle:{
					value: 360,
					duration:2000,
					repeat:30
				},
				scale:{
					value: {from: 0.40, to: 0.35},
					duration:1000,
					yoyo:true,
					repeat:-1
				},
			},
			
		})
	}

	allDestroy(){		
		this.children.iterate( (reward) =>{
			reward.destroy()
		})
	}


	distributeReward(){
		this.children.iterate( (reward) =>{
			if(!reward.catch){
				if(reward.y > this.scene.scale.height + 32){
					reward.y = Phaser.Math.Between(-700, -900)
					reward.x = Phaser.Math.Between( gameOptions.margin, this.scene.scale.width - gameOptions.margin)
				}
			}
		})

		if(this.getChildren().length <= 0){
			this.addReward( Phaser.Math.Between( gameOptions.margin, this.scene.scale.width - gameOptions.margin) , -32)					
		}		
	}

}

export default Reward
