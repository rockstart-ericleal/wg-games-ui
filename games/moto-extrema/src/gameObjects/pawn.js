class Pawn extends Phaser.Physics.Arcade.Group{
	constructor(config){
		super(config.physicsWorld, config.scene)
		this.addPawn()
		this.scene = config.scene
		this.texure = 'peon-1'
		this.level = config.level || 1	
		this.allChangeTexture(this.level)
	}

	addPawn(){
		let pawn = this.create(-100,-100,this.texure)
		pawn.dead = false
		pawn.setScale(0.25)
		pawn.name = 'Influenza'
		pawn.dead = false
		pawn.body.setCircle(54)
		pawn.body.setOffset(42, 42)
		pawn.toGo = false
		pawn.out = false
		pawn.setAlpha(1)

		this.scene.anims.create({
			key: this.texure,		    
		    frames: this.scene.anims.generateFrameNumbers(this.texure, { start: 0, end: 1 }),
		    frameRate: Phaser.Math.Between(3, 5),
		    repeat: -1
		});

		pawn.anims.play(this.texure)

	}

	allChangeTexture(level){

		let texture = 'peon-1'
		switch(level){
			case 1:
				texture = 'peon-1'
			break;
			case 2:
				texture = 'peon-2'
			break;
			case 3:
				texture = 'peon-3'
			break;
			case 4:
				texture = 'peon-4'
			break;
			default:
				texture = `peon-${Phaser.Math.Between(1, 4)}`
		}

		this.scene.anims.create({
			key: texture,		    
		    frames: this.scene.anims.generateFrameNumbers(texture, { start: 0, end: 1 }),
		    frameRate: Phaser.Math.Between(3, 5),
		    repeat: -1
		});
		
		this.children.iterate( (pawn) =>{
			pawn.anims.play(texture)
		})

		this.texure = texture
	}

}

export default Pawn
