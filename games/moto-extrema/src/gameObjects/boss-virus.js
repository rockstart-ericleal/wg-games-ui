import gameOptions from '../config.js'
class BossVirus extends Phaser.Physics.Arcade.Group{
	constructor(config){
		super(config.physicsWorld, config.scene)		
		this.scene = config.scene
		this.width = config.width
		this.height = config.height
		this.texture = 'boss-1'	
		this.level = config.level || 1	
	}	

	addboss(){
		let boss = this.create(Phaser.Math.Between( gameOptions.margin, this.width - gameOptions.margin),-100,this.texture)
		boss.dead = false
		boss.setScale(0.46)
		boss.name = 'Influenza'
		boss.dead = false
		boss.body.setCircle(68)
		boss.body.setOffset(36, 36)
		boss.toGo = false
		boss.setAlpha(1)
		boss.resistance = (Math.round(100 + (100 * this.level/3)))  
		boss.life = boss.resistance
		boss.army = 3

		this.scene.add.tween({
			targets: boss,
			y: { from: boss.y, to: 100 },
			x: gameOptions.margin,
			duration:400,
			onComplete: () => {
				this.scene.add.tween({					
					targets: boss,
					props:{
						x:{
							value: { from: gameOptions.margin, to: this.width - gameOptions.margin  },
							duration: 2000,
							repeat:-1,
							yoyo:true,
						},
						y:{
							value: { from: 100, to: this.height/4 },
							duration: 5000,
							repeat:-1,
							yoyo:true,
						},			
					},		
				})
			}
		})	

		this.allChangeTexture(this.level)

	}

	allChangeTexture(level){

		let texture = 'boss-1'
		switch(level){
			case 1:
				texture = 'boss-1'
			break;
			case 2:
				texture = 'boss-2'
			break;
			case 3:
				texture = 'boss-3'
			break;
			case 4:
				texture = 'boss-4'
			break;
			default:
				texture = `boss-${Phaser.Math.Between(1, 4)}`
		}

		this.scene.anims.create({
			key: texture,		    
		    frames: this.scene.anims.generateFrameNumbers(texture, { start: 0, end: 2 }),
		    frameRate: Phaser.Math.Between(3, 5),
		    repeat: -1
		});
		
		this.children.iterate( (boss) =>{
			boss.anims.play(texture)
		})

		this.texure = texture
	}

	allDestroy(){		
		this.children.iterate( (boss) =>{
			boss.destroy()
		})
	}

}

export default BossVirus
