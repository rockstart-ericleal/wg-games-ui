import gameOptions from '../config.js'
class BulletVirus extends Phaser.Physics.Arcade.Group{
	constructor(config){
		super(config.physicsWorld, config.scene)		
		this.scene = config.scene
		this.width = config.width
		this.height = config.height
	}

	addBullet(x, y){
		let bullet = this.create(x, y,'bullet-virus')
		bullet.setScale(0.5)
		bullet.setVelocityY(400)
		bullet.dead = false
	}
}

export default BulletVirus
