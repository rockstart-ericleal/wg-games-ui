import gameOptions from '../config.js'
class General extends Phaser.Physics.Arcade.Group{
	constructor(config){
		super(config.physicsWorld, config.scene)		
		this.scene = config.scene
		this.width = config.width
		this.height = config.height
		this.texture = 'general-1'	
		this.level = config.level || 1	

		this.addGeneral()
	}

	addGeneral(){
		let general = this.create(Phaser.Math.Between( gameOptions.margin, this.width - gameOptions.margin),-100,this.texture)
		general.dead = false
		general.setScale(0.48)
		general.name = 'Influenza'
		general.dead = false
		general.body.setCircle(54)
		general.body.setOffset(46, 46)
		general.toGo = false
		general.setAlpha(1)
		general.resistance = 7

		this.scene.add.tween({
			delay: Phaser.Math.Between(3000, 5000),
			targets: general,
			props:{
				x:{
					value: { from: general.x - 36, to: general.x + 36  },
					duration: 5000,
					repeat:-1,
					yoyo:true,
				},
				y:{
					value: { from: general.y, to: this.height },
					duration: 10000,
					repeat:-1,					
				},			
			},
		
		})

		this.allChangeTexture(this.level)

	}

	allChangeTexture(level){

		let texture = 'general-1'
		switch(level){
			case 1:
				texture = 'general-1'
			break;
			case 2:
				texture = 'general-2'
			break;
			case 3:
				texture = 'general-3'
			break;
			case 4:
				texture = 'general-4'
			break;
			default:
				texture = `general-${Phaser.Math.Between(1, 4)}`
		}

		this.scene.anims.create({
			key: texture,		    
		    frames: this.scene.anims.generateFrameNumbers(texture, { start: 0, end: 2 }),
		    frameRate: Phaser.Math.Between(3, 5),
		    repeat: -1
		});
		
		this.children.iterate( (general) =>{
			general.anims.play(texture)
		})

		this.texure = texture
	}

	allDestroy(){
		
		this.children.iterate( (general) =>{
			general.destroy()
		})
	}

}

export default General
