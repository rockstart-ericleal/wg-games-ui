import gameOptions from '../config.js'
class Bullet extends Phaser.Physics.Arcade.Group{
	constructor(config){
		super(config.physicsWorld, config.scene)		
		this.scene = config.scene
		this.texture = 'bullet'
		this.level = config.level || 1			
	}

	addBullet(x, y){
		let bullet = this.create(x, y, 'bullet')
		bullet.setImmovable(true)
		bullet.setScale(0.3)
		bullet.killed = true
		bullet.body.setSize(21, 12)
		bullet.body.setOffset(18, 15)
		
	}

	setTexture(texture){		
		this.children.iterate( (bullet) =>{
			bullet.setTexture(texture)
		})

		this.texture = texture
	}

	clearTexture(){
		if(this.texture === "bullet-2"){
			this.setTexture('bullet')
		}
	}


}

export default Bullet
