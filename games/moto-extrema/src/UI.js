import gameOptions from './config.js'
import { _$, log, metadata, getAutoHeight, shadowDistance, getScaleFitFlyer, saveDataServer } from "../../src/utils.js"

var LIFES = gameOptions.initLife
var levelGame = gameOptions.level

class UI extends Phaser.Scene {
	constructor(){
		super({key:'UI'})
	}
	init(){
		// log('Init UI')
		this.scene.launch('Moto')	
	}
	preload(){

		this.load.path = './games/moto-extrema/assets/'
		
		
		
		
	}
	create(){
		let { width, height } = this.sys.game.canvas;

		this.userCurrentScore = parseInt(gameOptions.miScore)
		this.passBarControler = parseInt(gameOptions.miScore)
		//Create Audio
		this.click = this.sound.add('explosion-shine')
		this.coins = this.sound.add('coins')
		this.nextLevel = this.sound.add('next-level-up')

		this.flags = {
			autoScore: true,
			bestScore: true
		}

		let cameraMain = this.cameras.main
		
		let rnd = new Phaser.Math.RandomDataGenerator((Date.now() * Math.random()).toString());		
		
		// for (let i = 0; i < 5; i++){
		// 	let decorationFlame = this.add.image(cameraMain.width*2 - 100, Phaser.Math.Between(0, cameraMain.height), 'UI', 'icon-flame');
		// 	decorationFlame.setScale( (0.3 + rnd.frac()) );
		// 	let speed = Phaser.Math.Between(2000, 6000)
		// 	let delay = Phaser.Math.Between(1000, 4000)

		// 	this.add.tween({
		// 		targets:[decorationFlame],
		// 		duration: speed,
		// 		yoyo:false,
		// 		x: {from: cameraMain.width + 100, to: -100},
		// 		alpha: {from: 0.7, to: 0},
		// 		repeat:-1,
		// 		delay:delay
		// 	})
		// }


		this.btnConnect = this.add.image(cameraMain.width - 12, -40, 'UI', 'btn-connect')
		this.btnConnect.setScale(0.75)
		this.btnConnect.setOrigin(1, 0)
		this.btnConnect.setInteractive()



		let styleUserScore = {
			strokeThickness: 3,
			stroke: '#000',
			color: '#fff', 			
			align: 'center',
			fontFamily: "'Geogrotesque Medium'", 
			fontSize: '32px',			
		}
		this.userScore = this.add.text(-(this.scale.width-32), 120, "0", styleUserScore)
		this.userScore.setOrigin(0.5)


		let styleLevelScore = {
			strokeThickness: 3,
			stroke: '#000',
			color: '#fff', 			
			align: 'center',
			fontFamily: "'Geogrotesque Medium'", 
			fontSize: '21px',			
		}
		this.levelText = this.add.text(cameraMain.width/2, cameraMain.height - 32, `NIVEL ${levelGame}`, styleLevelScore)
		this.levelText.setOrigin(0.5)
		this.levelText.setAlpha(0.7)
	

		// this.btnPause = this.add.image(this.scale.width + 45, 78, 'UI', 'btn-pause')
		// this.btnPause.setScale(0.6)
		// this.btnPause.setOrigin(0.5, 0)
		// this.btnPause.setInteractive()

		//log(gameOptions.bestScore, gameOptions.miScore)

		this.containerBestscore = this.add.container(48, 16)
		this.bestScoreText = this.add.text(0, 6, gameOptions.miScore, {fontSize: '18px', align: 'left', fontFamily: "'Geogrotesque Medium'"})
		this.bestTagScoreText = this.add.text(0, 26, "Tu mejor score", {fontSize: '12px', align: 'left', fontFamily: "'Geogrotesque Medium'"})
		this.btnIconBestScore = this.add.image(-21, 21, 'UI', 'icon-best-score')
		this.btnIconBestScore.setScale(0.7)

		this.containerBestscore.add([
			this.bestScoreText,
			this.bestTagScoreText,
			this.btnIconBestScore
		])

		this.groupHeartLife = this.add.group()
		this.contanerLifes = this.add.container(-200, 0)
		this.addLifeDisplay()
		

		//this.groupHeartLife.setOrigin(1,0.5)
			

		this.containerKingscore = this.add.container(48, 76)
		this.kingScoreText = this.add.text(0, 6, gameOptions.bestScore, {fontSize: '18px', align: 'left', fontFamily: "'Geogrotesque Medium'"})
		this.kingTagScoreText = this.add.text(0, 26, "Score a vencer", {fontSize: '12px', align: 'left', fontFamily: "'Geogrotesque Medium'"})
		this.btnIconKingScore = this.add.image(-21, 21, 'UI', 'icon-king-score')
		this.btnIconKingScore.setScale(0.7)

		this.containerKingscore.add([
			this.kingScoreText,
			this.kingTagScoreText,
			this.btnIconKingScore
		])


		// UI Start Game

		// this.containerStartGame = this.add.container(cameraMain.width/2,cameraMain.height/2)
		// this.pixelLayerBackground = this.add.image(0, 0, 'UI', 'pixel')
		// this.pixelLayerBackground.setDisplaySize(cameraMain.width, cameraMain.height)
		// this.pixelLayerBackground.setAlpha(0.7)


		// // this.headerTextStartGame = this.add.image(0, 0, 'UI', 'header-text-start-game').setScale(0.5)
		// // this.headerIconHand = this.add.image(0, 0, 'UI', 'icon-hand').setScale(0.5)
		// // this.headerIconHand.setScale(0)

		

		// this.iconPlayButtom = this.add.image(0, 0, 'UI', 'icon-play-button').setScale(0.5)
		// this.iconPlayButtom.y = 180
		// this.iconPlayButtom.setInteractive()

		// this.iconTextJugar = this.add.image(0, 0, 'UI', 'icon-text-jugar').setScale(0.5)
		// this.iconTextJugar.y = 300

		// this.add.tween({
		// 	targets:[this.iconTextJugar, this.iconPlayButtom],
		// 	ease: 'Cubic.easeInOut',
		// 	scale: { from: .45, to: .5 },
		// 	repeat:-1,
		// 	yoyo:true,
		// 	duration: 2000,
		// })

		// this.subContainerStartGame = this.add.container(0,-cameraMain.width/2)
		// this.subContainerStartGame.add([			
		// 	// this.headerTextStartGame,
		// 	// this.headerIconHand,
		// 	this.iconPlayButtom,
		// 	this.iconTextJugar
		// ])

		// this.containerStartGame.add([
		// 	this.pixelLayerBackground,
		// 	this.subContainerStartGame			
		// ])

		// this.iconPlayButtom.on(Phaser.Input.Events.POINTER_UP, ()=>{
		// 	this.registry.events.emit('loadTextScore', this.userScore.y)
		// 	this.click.play()
		// 	this.showConstrolsGame()
		// 	this.add.tween({
		// 		targets:[this.containerStartGame],				
		// 		duration:300,
		// 		ease:'Back.easeIn',
		// 		scale: 0,
		// 		onComplete: () =>{
		// 			this.registry.events.emit('geTextPoint', this.userScore.x,this.userScore.y )
		// 			this.registry.events.emit('startCapturePointer')					
		// 			this.time.addEvent({
		// 			    delay: 2000,                // ms
		// 			    callback: ()=>{ this.registry.events.emit('startCapturePointer')},		    
		// 			    callbackScope: this,
		// 			    repeat: 1
		// 			});
		// 			// this.headerIconHand.setScale(0.5)
		// 			// this.headerIconHand.x = cameraMain.width/2
		// 			// this.headerIconHand.y = cameraMain.height - (cameraMain.height/3)
		// 			// this.add.tween({
		// 			// 	targets:[this.headerIconHand],
		// 			// 	duration: 500,
		// 			// 	yoyo:true,
		// 			// 	y: this.headerIconHand.y + 21,
		// 			// 	x: this.headerIconHand.x + 21,
		// 			// 	repeat:-1

		// 			// })
		// 		}
		// 	});
			
		// })


		//UI Pause Game 

		// this.pixelLayerBackgroundPause = this.add.image(0, 0, 'UI', 'pixel')
		// this.pixelLayerBackgroundPause.setDisplaySize(cameraMain.width, cameraMain.height)
		// this.pixelLayerBackgroundPause.setAlpha(0.6)

		// this.iconPlayButtomPause = this.add.image(0, -32, 'UI', 'icon-play-button').setScale(0.32)
		// this.iconPlayButtomPause.setInteractive()

		// this.userNameText = this.add.text(0, 64, gameOptions.userName, {fontSize: '21px', align: 'left', fontFamily: "'Geogrotesque Medium'"}).setOrigin(0.5)
		// this.userCurrenScoreText = this.add.text(0, 94, "TU MEJOR SCORE: " + gameOptions.miScore, {fontSize: '21px', align: 'left', fontFamily: "'Geogrotesque Medium'"}).setOrigin(0.5)

		// this.containerPauseGame = this.add.container(cameraMain.width/2, cameraMain.height/2)
		// this.containerPauseGame.add([			
		// 	this.pixelLayerBackgroundPause,
		// 	this.add.image(0, 0, 'UI', 'bg-continue').setScale(0.4),
		// 	this.userNameText,
		// 	this.userCurrenScoreText,
		// 	this.iconPlayButtomPause
		// ])

		// this.add.tween({
		// 	targets:[this.iconPlayButtomPause],
		// 	ease: 'Cubic.easeInOut',
		// 	scale: { from: .30, to: .25 },
		// 	repeat:-1,
		// 	yoyo:true,
		// 	duration: 1000,
		// })	


		// this.containerPauseGame.setScale(0)

		// this.btnPause.on(Phaser.Input.Events.POINTER_UP, ()=>{
		// 	this.click.play()
		// 	this.hideConstrolsGame()
		// 	this.add.tween({
		// 		targets:[this.containerPauseGame],				
		// 		duration:300,
		// 		ease:'Back.easeIn',
		// 		scale: 1,
		// 		onComplete: () =>{
		// 			this.scene.pause('Jetpack')
		// 		}
		// 	});
		// })

		// this.iconPlayButtomPause.on(Phaser.Input.Events.POINTER_UP, ()=>{
		// 	this.click.play()
		// 	this.showConstrolsGame()
		// 	this.add.tween({
		// 		targets:[this.containerPauseGame],				
		// 		duration:300,
		// 		ease:'Back.easeIn',
		// 		scale: 0,
		// 		onComplete: () =>{
		// 			this.scene.resume('Jetpack')
		// 		}
		// 	});
			
		// })



		this.pixelLayerBackgroundConnect = this.add.image(0, 0, 'UI', 'pixel')
		this.pixelLayerBackgroundConnect.setDisplaySize(cameraMain.width, cameraMain.height)
		this.pixelLayerBackgroundConnect.setAlpha(0.6)

		this.preloadConnect = this.add.image(0, 26, 'UI', 'preload')
		this.preloadConnect.setScale(0.3)

		this.add.tween({
			targets:[this.preloadConnect],				
			duration:6000,
			ease:'Linear',
			rotation: 45,
			repeat:-1
		});

		this.containerConnectGame = this.add.container(cameraMain.width/2, cameraMain.height/2)
		this.containerConnectGame.add([
			this.pixelLayerBackgroundConnect,
			this.add.image(0, 0, 'UI', 'bg-connect').setScale(0.4),
			this.preloadConnect
		])

		this.containerConnectGame.setScale(0)

		this.btnConnect.on(Phaser.Input.Events.POINTER_UP, ()=>{
			
			// this.hideConstrolsGame()
			// this.add.tween({
			// 	targets:[this.containerConnectGame],				
			// 	duration:300,
			// 	ease:'Back.easeIn',
			// 	scale: 1,
			// 	onComplete: () =>{
			// 		log('Conectando...')
			// 	}
			// });
			this.toConnect()
		})

		this.setRegistryEvents()
		//this.removeHeartLife()


		this.time.addEvent({
			delay:10000,
			callback: () => { this.saveDataGame() },
			callbackScope: this,
			loop:true

		})

	}


	toConnect(){
		this.scene.remove('Moto')
		this.click.play()
		this.registry.events.emit('toConnectWifi')
		this.saveDataGame()
	}

	addLifeDisplay(){

		// log('UI lifes addLifeDisplay', LIFES)

		for (let i = 0; i < this.groupHeartLife.getChildren().length; i++){
			this.groupHeartLife.getChildren()[i].destroy()
		}
		this.contanerLifes.removeAll()
		let tempH = 0
		for (let i = 0; i < LIFES; i++){
			let heart = this.groupHeartLife.create(this.scale.width-32, 84, 'UI', 'icon-heart')
			heart.setScale(0.5)
			heart.x = tempH
			this.contanerLifes.add([
				heart
			])
			tempH -= 32
		}

		if (LIFES <= 0){
			this.click.play()
			this.scene.pause('Moto')
			this.hideConstrolsGame()
			this.toConnect()
		}
		
	}

	saveDataGame(){
		let data = {
			'token': gameOptions.token,
			'best_score': gameOptions.bestScore,
			'current_score': this.userCurrentScore,
			'level': levelGame,
			'anuncio_id': gameOptions.anuncioId,
			'game_name': gameOptions.gameName,
			'anuncio_game_id': gameOptions.anuncioGameId,
			'user_id': gameOptions.userId
		}
		saveDataServer(data)
	}

	showDialogNextLevel(level){
		
		//levelGame = levelGame + 1

		// this.pixelLayerNextBackground = this.add.image(0, 0, 'UI', 'pixel')
		// this.pixelLayerNextBackground.setDisplaySize(this.scale.width, this.scale.height)
		// this.pixelLayerNextBackground.setAlpha(0.7)

		// this.bgContinueNext = this.add.image(0, 0, 'UI', 'bg-next-level').setScale(0.4)

	 //    this.texNextLevel = this.add.text(0, -115, `NIVEL ${levelGame}`, {color: '#783000',fontSize: '64px', align: 'left', fontFamily: "'Geogrotesque Medium'"}).setOrigin(0.5)
	 //    this.scoreCurrentTotal = this.add.text(0, -64, `SCORE: ${this.userCurrentScore}`, {fontSize: '18px', align: 'left', fontFamily: "'Geogrotesque Medium'"}).setOrigin(0.5)

	 //    this.iconContinueButtom = this.add.image(0, 0, 'UI', 'btn-coninue-next').setScale(0.4)
		// this.iconContinueButtom.y = 160
		// this.iconContinueButtom.setInteractive()



		// this.add.tween({
		// 	targets:[this.iconContinueButtom],
		// 	ease: 'Cubic.easeInOut',
		// 	scale: { from: .45, to: .38 },
		// 	repeat:-1,
		// 	yoyo:true,
		// 	duration: 1000,
		// })

	 //    this.containerNextLevel = this.add.container(this.scale.width/2, this.scale.height/2)
		// this.containerNextLevel.add([
		// 	this.pixelLayerNextBackground,
		// 	this.bgContinueNext,
		// 	this.iconContinueButtom,
		// 	this.texNextLevel,
		// 	this.scoreCurrentTotal
		// ])
		// this.containerNextLevel.setScale(0)

		// this.add.tween({
		// 	targets:[this.containerNextLevel],
		// 	ease: 'Cubic.easeInOut',
		// 	scale: { from: 0, to: 1 },
		// 	repeat:0,
		// 	yoyo:false,
		// 	duration: 300,
		// })

		// this.iconContinueButtom.on(Phaser.Input.Events.POINTER_UP, () => {
		// 	this.add.tween({
		// 		targets:[this.containerNextLevel],
		// 		ease: 'Cubic.easeInOut',
		// 		scale: { from: 1.2, to: 0 },
		// 		repeat:0,
		// 		yoyo:false,
		// 		duration: 300,
		// 		onComplete: () =>{
		// 			this.registry.events.emit('startNextGame')
		// 		}	
		// 	})
		// });		

		this.add.tween({
			targets:[this.levelText.text],
			ease: 'Cubic.easeInOut',
			scale: { from: 1.5, to: 1 },
			repeat:0,
			yoyo:false,
			duration: 600,
			onComplete: () => {
				this.levelText.text = `NIVEL ${level}`
			}
		})

		//this.scene.pause('Jetpack')
		this.nextLevel.play()
		this.saveDataGame()
	}


	setRegistryEvents(){
		this.registry.events.on('gameScoreNotify', (score) =>{
			this.setCurrentScore(score)
			if (score == 0){
				this.userCurrentScore = 0
				this.userScore.text = "0"
			}
		})

		this.registry.events.on('startGameInit', () => {
			//this.headerIconHand.setScale(0)
		})

		this.registry.events.on('gameOverNotify', () =>{
			//this.removeHeartLife()
			LIFES--
			// log('UI lifes gameOverNotify', LIFES)
			this.addLifeDisplay()
		})

		this.registry.events.on('updateLevelControl', () =>{
			this.passBarControler += 1
		})

		this.registry.events.on('updateLevelText', (level) =>{
			this.showDialogNextLevel(level)
		})

		this.registry.events.on('rewardLife', (level) =>{			
			LIFES++
			this.addLifeDisplay()			
		})

		this.registry.events.on('startGame', ()=>{
			this.registry.events.emit('loadTextScore', this.userScore.y)
			this.click.play()
			this.showConstrolsGame()
			this.registry.events.emit('geTextPoint', this.userScore.x,this.userScore.y )
			this.registry.events.emit('startCapturePointer')					
			// this.time.addEvent({
			//     delay: 2000,                // ms
			//     callback: ()=>{ this.registry.events.emit('startCapturePointer')},		    
			//     callbackScope: this,
			//     repeat: 1
			// });
		})

	}

	//"image": "./images/data-images.png",
	setCurrentScore(point){
		this.userCurrentScore += point
		this.userScore.text = this.userCurrentScore

		this.add.tween({
			targets: this.userScore,
			repeat: 0,
			ease: 'Elastic',
			duration: 300,
			scale: {from: 1, to: 0.8 },
			yoyo:false

		})

		if(parseInt(gameOptions.miScore) < this.userCurrentScore){
			
			this.bestScoreText.text = this.userCurrentScore
			//this.userCurrenScoreText.text = "TU MEJOR SCORE: " + this.userCurrentScore

			if (this.flags.autoScore){
				this.flags.autoScore = false
				//let particles = this.add.particles('UI', 'icon-best-score');
				//let rect = new Phaser.Geom.Rectangle(0, 0, this.scale.width, this.scale.height);
			    // particles.createEmitter({
			        
			    //     x: this.btnIconBestScore.x + 50, y: this.btnIconBestScore.y + 20,
			    //     lifespan: 4000,
			    //     speed: { min: 100, max: 250 },
			    //     scale: { start: 1, end: 0.2 },
			    //     gravityY: 150,
			    //     bounce: 0.8,
			    //     bounds: rect,
			    //     blendMode: 'ADD',
			    //     frequency: 110,
			    //     maxParticles: 12,
			    // });
			    
			    this.coins.play()
			}						
		}

		if(parseInt(gameOptions.bestScore) < this.userCurrentScore){
						
			this.kingScoreText.text = this.userCurrentScore

			if (this.flags.bestScore ){
				this.flags.bestScore = false
				//let particles = this.add.particles('UI', 'icon-min-star');
				//let rect = new Phaser.Geom.Rectangle(0, 0, this.scale.width, this.scale.height);
			    // particles.createEmitter({
			        
			    //     x: this.btnIconKingScore.x + 50, y: this.btnIconKingScore.y + 80,
			    //     lifespan: 4000,
			    //     speed: { min: 100, max: 250 },
			    //     scale: { start: 1, end: 0.2 },
			    //     gravityY: 150,
			    //     bounce: 0.8,
			    //     bounds: rect,
			    //     blendMode: 'ADD',
			    //     frequency: 110,
			    //     maxParticles: 12,
			    // });
			    // this.saveDataGame()
			    this.coins.play()
			}
		}


		if ( this.userCurrentScore % 2 == 0 ){
			// this.saveDataGame()
		}

		if(
			this.passBarControler == 10 ||
			this.passBarControler == 30 ||
			this.passBarControler == 50 ||
			this.passBarControler == 80 ||
			this.passBarControler == 160 ||
			this.passBarControler == 300 ||
			this.passBarControler == 400 ||
			this.passBarControler == 500
		){
			this.passBarControler += 2
			
		}

	}

	// removeHeartLife(){
	// 	this.passBarControler = parseInt(gameOptions.miScore)
	// 	lifes = lifes - 1
	// 	if (lifes >= 0){
	// 		let heart = this.groupHeartLife.getChildren()[lifes]
	// 		this.add.tween({
	// 			targets:[heart],				
	// 			duration:500,
	// 			ease:'Quart.easeInOut',
	// 			scale:{from: 1.001, to: 0},
	// 			onComplete: () =>{
	// 				heart.destroy()
	// 			}
	// 		});
	// 	}

	// 	if (lifes <= 0){
	// 		this.click.play()
	// 		this.scene.pause('Moto')
	// 		this.hideConstrolsGame()
	// 		this.add.tween({
	// 			targets:[this.containerConnectGame],				
	// 			duration:300,
	// 			ease:'Back.easeIn',
	// 			scale: 1,
	// 			onComplete: () =>{
	// 				log('Conectando...')
	// 			}
	// 		});
	// 	} 

	// 	this.addLifeDisplay(lifes)
	// }

	showConstrolsGame(){
		this.add.tween({
			targets:[this.userScore],				
			duration:300,
			ease:'Back.easeIn',
			x:{from: this.scale.width + 32, to:this.scale.width - 32}
		});

		this.add.tween({
			targets:[this.btnConnect],				
			duration:300,
			ease:'Back.easeIn',
			y:{from: -40, to:20}
		});

		this.add.tween({
			targets: this.contanerLifes,				
			duration: 300,
			ease:'Back.easeIn',
			x:{from: this.scale.width+200, to: this.scale.width-32},				
		});

	
	}

	hideConstrolsGame(){
		this.add.tween({
			targets:[this.userScore],				
			duration:300,
			ease:'Back.easeIn',
			x:{from: this.scale.width - 32, to:this.scale.width + 32}
		});

		this.add.tween({
			targets:[this.btnConnect],				
			duration:300,
			ease:'Back.easeIn',
			y:{from: -200, to:-40}
		});

		this.add.tween({
			targets: this.contanerLifes,				
			duration: 300,
			ease:'Back.easeIn',
			x:{from: this.scale.width -(this.contanerLifes.displayWidth + 32), to: this.scale.width+200},						
		});
	}

}

export default UI