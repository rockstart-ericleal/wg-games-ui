import gameOptions from './config.js'
import { _$, log, metadata } from "../../src/utils.js"
import Pawn from 'gameObjects/pawn.js'
import General from 'gameObjects/general.js'
import BulletVirus from 'gameObjects/bullet-virus.js'
import BossVirus from 'gameObjects/boss-virus.js'
import Reward from 'gameObjects/reward.js'
import Bullet from 'gameObjects/bullet.js'
import Life from 'gameObjects/life.js'

var floatX = 0
var floatY = 0

var PLAY_GAME = true
var SHOW_BOSS_LEVEL = false
var LEVEL = 1
var controlScoreLevel = 0

var lifes = gameOptions.initLife

class Moto extends Phaser.Scene{
	constructor(){
		super("Moto");
	}

	init(){
		// log('Init Moto')
		this.events.on('pause', function(){ 
			this.scene.setVisible(false, 'Moto');
		}, this);
		this.events.on('resume', function(){ 
			this.scene.setVisible(true, 'Moto');
		}, this);
	}

	preload(){

		//this.load.path = './games/moto-extrema/assets/'
		
		
		
		

	}

	create(){

		this.userScore = 0
		this.exploitGroup = this.add.group()


		for (let i = 0; i < 3; i ++){
			let exploit = this.exploitGroup.create(-100, -100, 'exploit-'+i)
			this.anims.create({
				key: 'exp-'+i,		    
			    frames: this.anims.generateFrameNames('exploit-'+i, { 
			    	prefix: 'xp'+i+'-',
			    	start: 0,
			    	end: 63	    
			    }),
			    frameRate: 64,
			    repeat: 0
			});

			exploit.enabled = true
		}
		

		
		this.velocity = 3
		this.width = this.scale.width
		this.height = this.scale.height
		this.moveMoto = 400
		this.groupTotalBullets = 20
		this.roadGroup = this.add.group()
		
		let controlHeight = this.height
		for (let i = 0; i < 2; i++){
			
			let road = this.add.image(this.width/2,controlHeight, 'road').setScale(0.6)
			road.setOrigin(0.5,1)			
			road.setAlpha(0)
			this.roadGroup.add(road)
			
			// log('displayHeight', road.displayHeight)
			// log('controlHeight', controlHeight)
			controlHeight = 0
		}
		this.resetRoadY = ( this.height - this.roadGroup.getChildren()[0].displayHeight )
		this.roadGroup.getChildren()[1].y = this.resetRoadY

		this.rectloader = this.add.graphics();
		this.progressBar = this.add.graphics();

		this.shootSound = this.sound.add('shot-1')
		this.shootSound.volume = 0.2

		this.shootVirusSound = this.sound.add('shot-2')
		this.shootVirusSound.volume = 0.4

		this.motoPainSound = this.sound.add('moto-pain')
		this.motoPainSound.volume = 0.5

		this.popSound = this.sound.add('pop-1')
		this.popSound.volume = 0.2

		this.evilSound = this.sound.add('evil-sound')
		this.evilSound.volume = 0.5

		this.rewardSound = this.sound.add('winner-1')
		this.rewardSound.volume = 0.5

		this.nextLevelSound = this.sound.add('next-level-up')
		this.nextLevelSound.volume = 0.5

		this.nextLevelSound = this.sound.add('pistol')
		this.nextLevelSound.volume = 0.5


		this.moto = this.physics.add.image(this.width/2, this.height + 200, 'moto')
		this.moto.setScale(0.45)
		this.moto.posX = 0
		this.moto.infect = false

		this.moto.setSize(100, 140);
		this.moto.setOffset(0, 120);

		this.add.tween({
			targets:[this.moto],
			duration: 1000,
			y: (this.height - (this.height/7)),
			delay:100,
			ease:'Expo.easeInOut',
			onComplete: () => {
				this.add.tween({
					targets:[this.moto],
					duration: 3000,
					y: this.moto.y + 21,
					repeat:-1,
					yoyo: true,
					ease:'Linear'
				})
			}
		})

		this.bulletGroup = new Bullet({
			physicsWorld: this.physics.world,
			scene: this,
			width:this.width,
			height:this.height,
			level: LEVEL
		})

		for (let i = 0; i < this.groupTotalBullets; i++){
			this.bulletGroup.addBullet(this.moto.x, this.moto.y)
		}

		


		this.input.addPointer(2)
		this.pointer1 = this.input.pointer1	
		this.pointer2 = this.input.pointer2
		this.pointer3 = this.input.pointer3

		this.controFire = 0
		this.adwarBullet = 0

		this.pawnGroup = this.physics.add.group()
		this.pawnGroup = new Pawn({
			physicsWorld: this.physics.world,
			scene: this
		})
		
		this.rectLimit = this.physics.add.image(this.width/2, this.height +5, 'pixel')
		this.rectLimit.setDisplaySize(this.width, 5)

		this.physics.add.collider(this.rectLimit, this.pawnGroup, (rec, enemy) =>{			
			if(!enemy.out){
				      			
				if(this.userScore > 1){
					log('HERE', 'HERE!!!')
					this.setScore(-2)
				}
				enemy.out = true
				enemy.destroy()
				this.pawnGroup.addPawn()
      		}
		})

		this.generalGroup = new General({
			physicsWorld: this.physics.world,
			scene: this,
			width:this.width,
			height:this.height,
			level: LEVEL
		})
		


		this.bulletVirusGroup = new BulletVirus({
			physicsWorld: this.physics.world,
			scene: this,
			width:this.width,
			height:this.height,
			level: LEVEL
		})



		this.bossVirusGroup = new BossVirus({
			physicsWorld: this.physics.world,
			scene: this,
			width:this.width,
			height:this.height,
			level: LEVEL
		})

		this.rewardGroup = new Reward({
			physicsWorld: this.physics.world,
			scene: this,
			width:this.width,
			height:this.height,
			level: LEVEL
		})

		this.lifeGroup = new Life({
			physicsWorld: this.physics.world,
			scene: this,
			width:this.width,
			height:this.height,
			level: LEVEL
		})

		

		//Empezar desde la derecha
        this.startPivot = true
        this.xDistance = this.initDistance()
		//this.injectEnemy()

		for (let i = 0; i < 7; i ++){
			this.pawnGroup.addPawn()
		}

		let rect = new Phaser.Geom.Rectangle(0, 0, this.scale.width, this.scale.height);

		this.physics.add.overlap(this.rewardGroup, this.moto, (bullet, reward) => {        	 
      		if(!reward.catch){
      			this.nextLevelSound.play()
      			this.adwarBullet += 50
      			this.bulletGroup.setTexture('bullet-2')
				reward.catch = true 
				this.rewardGroup.allDestroy()				
										
      		}
        })

        this.physics.add.overlap(this.lifeGroup, this.moto, (bullet, life) => {        	 
      		if(!life.catch){
      			this.rewardSound.play()
				life.catch = true 
				this.lifeGroup.allDestroy()
				this.registry.events.emit('rewardLife') 
				lifes++ 							
      		}
        })

		this.physics.add.overlap(this.bulletGroup, this.pawnGroup, (bullet, enemy) => {        	 
      		if(!enemy.dead && enemy.y > 5){
      			this.setScore(1)
      			enemy.dead = true
      			this.showExploit(bullet.x, bullet.y, enemy)
      			bullet.x = -500
      			//log('DEAD!!!!!', enemy)   
      			this.popSound.play()   			
      		}
        })


		this.physics.add.overlap(this.bulletGroup, this.generalGroup, (bullet, general) => {        	 
      		if(!general.dead && general.y > 5){  
      			this.setScore(1)   			
      			this.showExploit(bullet.x, bullet.y+10, null)      			
      			bullet.x = -500
      			general.resistance--
      			general.resistance

      			if (general.resistance <= 0){
      				general.dead = true
      				
      				this.add.tween({
      					ease: 'Bounce',
      					targets: general,
      					scale:{ from: 0.20, to: .36},
      					alpha:{ from: 1, to: 0},
      					yoyo: false,
      					duration: 300,
      					onComplete: ()=> {      						      						
      						general.destroy()
      						this.generalGroup.addGeneral()
      						this.generalGroup.allChangeTexture(LEVEL)

      					}

      				})

      			}
      			this.popSound.play()			
      		}
        })

		
		this.physics.add.collider([this.pawnGroup, this.generalGroup, this.bulletVirusGroup], this.moto, (moto, objCollision) => {

			log('objCollision.texture.key', objCollision.texture.key)

			if (!this.moto.infect){

				if(
					objCollision.texture.key == 'peon-1' ||
					objCollision.texture.key == 'peon-2' ||
					objCollision.texture.key == 'peon-3' ||
					objCollision.texture.key == 'peon-4'
				){
					objCollision.destroy()
					this.pawnGroup.addPawn()
				}

				if(
					objCollision.texture.key == 'general-1' ||
					objCollision.texture.key == 'general-2' ||
					objCollision.texture.key == 'general-3' ||
					objCollision.texture.key == 'general-4'
				){
					objCollision.destroy()
					this.generalGroup.addGeneral()
					this.generalGroup.allChangeTexture(LEVEL)
				}


				if (
					objCollision.texture.key == 'peon-1' ||
					objCollision.texture.key == 'peon-2' ||
					objCollision.texture.key == 'peon-3' ||
					objCollision.texture.key == 'peon-4' || 
					objCollision.texture.key == 'bullet-virus' || 
					objCollision.texture.key == 'general-1' ||
					objCollision.texture.key == 'general-2' ||
					objCollision.texture.key == 'general-3' ||
					objCollision.texture.key == 'general-4'

					){
 

					objCollision.dead = true					
					let particles = this.add.particles(objCollision.texture.key);							
				    particles.createEmitter({
				     
				        x: objCollision.x, y: objCollision.y,
				        lifespan: 4000,
				        speed: { min: 60, max: 80 },
				        scale: { start: 0.12, end: 0 },
				        gravity: 0,			        
				        bounce: 0.9,
				        bounds: rect,
				        // blendMode: 'ADD',
				        frequency: 8,
				        maxParticles: 10,
				    });
				    objCollision.setAlpha(0)
				}

				if (objCollision.texture.key == "bullet-virus"){
					objCollision.destroy()
				}
	

				this.moto.infect = true
				this.motoPainSound.play()
				this.registry.events.emit('gameOverNotify')
				lifes--
				this.time.addEvent({
				    delay: 1300,               
				    callback: ()=>{  this.moto.infect = false;},				    
				    callbackScope: this,
				    repeat: 0
				});
			}
			
		})


		this.physics.add.overlap(this.bulletGroup, this.bossVirusGroup, (bullet, boss) => {        	 
      		if(!boss.dead){ 

      			this.setScore(1)

      			this.showExploit(bullet.x, bullet.y+10, null)      			
      			bullet.x = -500
      			boss.resistance--      			      			
      			this.drawBossLife(boss.resistance, boss.life)
      			if (boss.resistance <= 0){
      				boss.dead = true
      				controlScoreLevel = 0
      				this.add.tween({
      					ease: 'Bounce',
      					targets: boss,
      					scale:{ from: 0.20, to: .36},
      					alpha:{ from: 1, to: 0},
      					yoyo: false,
      					duration: 300,
      					onComplete: ()=> {
      						boss.destroy()
      						this.clearBossLife()
      						this.generalGroup.addGeneral()
      						this.generalGroup.allChangeTexture(LEVEL) 	
      						this.nexLevel()					
      					}

      				})

      				SHOW_BOSS_LEVEL = false

      			} 

      			this.popSound.play()
      			
      		}

      		log('boss.resistance', boss.resistance)
        })

        let timer = this.time.addEvent({
		    delay: 1000,                // ms
		    callback: ()=>{  this.distributionEnemy();},
		    //args: [],
		    callbackScope: this,
		    repeat: -1
		});
		

        let timerGeneral = this.time.addEvent({
		    delay: 2000,                // ms
		    callback: ()=>{ this.fireGenralBullet() },		    
		    callbackScope: this,
		    repeat: -1
		});

		let timerBoss = this.time.addEvent({
		    delay: 1800,                // ms
		    callback: ()=>{ this.fireBossBullet() },		    
		    callbackScope: this,
		    repeat: -1
		});

		let timerLife = this.time.addEvent({
		    delay: 6000,                // ms
		    callback: ()=>{ this.addRewardLide() },		    
		    callbackScope: this,
		    repeat: -1
		});

		
		this.scene.pause('Moto')
		this.setRegistryEvents()
		this.registry.events.emit('updateLevelText', LEVEL)


		this.input.on('pointerdown', this.fireBullet, this);
		//this.input.on('pointerdown', ()=> { log('PONT', '0 PRESS!')} , this);

		
		
	}

	addRewardLide(){
		// log('LIFES', lifes)
		if( lifes < gameOptions.initLife){
			this.lifeGroup.distributelife();
		}
	}

	nexLevel(){
		LEVEL++
		this.nextLevelSound.play()
		this.pawnGroup.allChangeTexture(LEVEL)

		this.registry.events.emit('updateLevelText', LEVEL)
	}

	setScore(score){
		controlScoreLevel += score
		this.userScore += score
		this.registry.events.emit('gameScoreNotify', score)

		

		if(controlScoreLevel >= 200){
			controlScoreLevel = 0
			SHOW_BOSS_LEVEL = true
		}
	}

	fireBossBullet(){

		if(SHOW_BOSS_LEVEL){



			if(this.bossVirusGroup.getChildren().length == 0){
				this.bossVirusGroup.addboss()
				this.bossVirusGroup.allChangeTexture(LEVEL)
			}

			for(let i = 0; i < this.bossVirusGroup.getChildren().length; i++){
				let boss = this.bossVirusGroup.getChildren()[i]
				if(!boss.dead){
					if(boss.y > 0){

						for (let a = 0; a < boss.army; a++){
							this.bulletVirusGroup.addBullet(boss.x + Phaser.Math.Between(-30, 30), boss.y - Phaser.Math.Between(-30, 30) )
						}						
						this.shootVirusSound.play()
					}							
				}
			}

			this.evilSound.play()

		}else{
			this.bossVirusGroup.allDestroy()
		}	
	}


	fireGenralBullet(){
		for(let i = 0; i < this.generalGroup.getChildren().length; i++){
			let general = this.generalGroup.getChildren()[i]
			if(!general.dead){
				if(general.y > 0){
					this.bulletVirusGroup.addBullet(general.x, general.y)	
					this.shootVirusSound.play()
				}							
			}
		}


		this.rewardGroup.distributeReward()

		if(SHOW_BOSS_LEVEL){
			this.generalGroup.allDestroy()
		}		
	}

	distributionEnemy(){
		this.xDistanceEnemy()
		//this.showExploit(this.xDistance, 300)
		this.injectEnemy(this.xDistance)
		for (let i = 0; i < this.groupTotalBullets; i++){
			let bullet = this.bulletGroup.getChildren()[i]
			if(bullet.y <= 0){							
				bullet.killed = true
				bullet.setScale(0.3)
			} 			
		}
		
	}

	initDistance(){		
		return (this.startPivot)? this.width - gameOptions.margin : gameOptions.margin
	}

	xDistanceEnemy(){
		let distanceLeft = gameOptions.margin
		let distanceRight = this.width - gameOptions.margin 
		let distance = (this.width - (gameOptions.margin*2)) / gameOptions.totalEnemy

		let disTem
		if(this.xDistance < distanceRight+10 && this.xDistance > distanceLeft-10){
			// log('this.xDistance en rango', this.xDistance)

			if(this.startPivot){
				this.xDistance -= distance
			}else{
				this.xDistance += distance
			}

		}else{
			if (this.xDistance > distanceRight){
				// log('Ya me pase a la derecha')
				this.startPivot = true
				this.xDistance -= distance
			}
			if (this.xDistance < distanceLeft){
				// log('Ya me pase a la izquierda')
				this.startPivot = false
				this.xDistance += distance
			}
		}

		
		// log('this.xDistance', this.xDistance)
	}



	injectEnemy(x){				


		if (!SHOW_BOSS_LEVEL){
			let enemy = null
			this.pawnGroup.getChildren().reverse()
			for (let i = 0; i < this.pawnGroup.getChildren().length; i ++){
				if ( !this.pawnGroup.getChildren()[i].dead && !this.pawnGroup.getChildren()[i].toGo ){
					enemy = this.pawnGroup.getChildren()[i]
					enemy.toGo = true
					enemy.y = -100
					enemy.x = x
					enemy.setScale(0.25)
					enemy.setAlpha(1)
					enemy.dead = false								
					break

				}
			}
			
			if(enemy != null){						
				this.add.tween({
					targets: enemy,

					props:{
						x:{
							value: {from: enemy.x, to: enemy.x += 36},
							yoyo: true,
							repeat: -1,
							duration: 3000,
						},

						y: {
							ease:'Linear',
							value: {from: enemy.y, to: this.height + 60 },
							duration: Phaser.Math.Between(4000, 6000),
							repeat: -1,
							

						},
						
					},


				})
			}else{
				log('injectEnemy', 'No hay enemy')
			}
		}

		
	}

	showExploit(x, y, enemy){

		let exploit = null
		let indexAnims = 0
		for (let i = 0; i < this.exploitGroup.getChildren().length; i++){
			if (this.exploitGroup.getChildren()[i].enabled){
				exploit = this.exploitGroup.getChildren()[i]
				exploit.enabled = false
				indexAnims = i
				break
			}
		}

		if (exploit != null){
			exploit.x = x
			exploit.y = y - 10
			exploit.anims.play('exp-'+indexAnims)
			let timer = this.time.addEvent({
			    delay: 1000,                // ms
			    callback: ()=>{ exploit.enabled = true  },
			    //args: [],
			    callbackScope: this,
			    repeat: 0
			});
		}

		// log('this.exploitGroup.getChildren()', this.exploitGroup.getChildren())

		if(enemy != null){
			this.add.tween({
				targets: enemy,
				duration:200,
				ease:'Bounce',
				scale: {from: 0.20, to: .33 },
				alpha:{from: 1, to: 0},
				onComplete: () =>{
					enemy.destroy()
					this.pawnGroup.addPawn()
				},
				repeat: 0,
				yoyo:false
			})
		}
		
	}

	fireBullet(){
		
		if(this.adwarBullet <= 0){
			this.fire()
		}
		 
	}


	fire(){
		let bullet = this.getBullet()
		if(bullet != null){	
			bullet.x = this.moto.x + 14
			bullet.y = this.moto.y - 32
			bullet.setVelocityY(-400)
			bullet.killed = false						
			this.shootSound.play()
		}else{
			log('No hay bullets')
		}
	}

	getBullet(){
		for (let i = 0; i < this.groupTotalBullets; i++){
			let bullet = this.bulletGroup.getChildren()[i]
			if(bullet.killed){							
				return bullet
			} 			
		}
		return null
	}

	traslateMoto(x){
		// log('moveMoto', x)
		// log('Moto', this.moto.x)
		// log('Moto', this.moto)

		if( (x -(this.width/2)) < this.moto.posX){
			//log('this.moto.posX' , 'IZQUIERDA')
			this.moto.body.setVelocityX(-this.moveMoto)
		}else{
			//log('this.moto.posX' , 'DERECHA')		
			this.moto.body.setVelocityX(this.moveMoto)
		}
	}

	stopMoto(){
		this.moto.body.setVelocityX(0)
	}

	setRegistryEvents(){
		this.registry.events.on('startCapturePointer', ()=>{
			this.scene.resume('Moto')		
		})
	}

	runRoad(velocity){

		if(PLAY_GAME){
			this.roadGroup.getChildren()[0].y += velocity
			if(this.roadGroup.getChildren()[0].y >= (this.roadGroup.getChildren()[0].displayHeight + this.height) ){
				this.roadGroup.getChildren()[0].y = this.resetRoadY
			}

			this.roadGroup.getChildren()[1].y += velocity
			if(this.roadGroup.getChildren()[1].y >= (this.roadGroup.getChildren()[1].displayHeight + this.height) ){
				this.roadGroup.getChildren()[1].y = this.resetRoadY
			}	
		}

		if (this.moto.x > this.width){
			this.moto.x = this.width -10
		}
		

		if (this.moto.x <= 0){
			this.moto.x = 10
		}


		
	}
	
	update(time, delta){

		this.controFire = (this.controFire >= 100)? 0 : (this.controFire += 1)
		this.moto.posX = -(this.width/2) + this.moto.x
		//log('this.moto.posX', this.moto.posX)
		//this.runRoad(this.velocity)

		// if (this.pointer3.isDown){			
		// 	this.traslateMoto(this.pointer3.x)
		// 	this.superBullet(this.controFire)
		// }
				

		if (this.pointer1.isDown){
			//this.registry.events.emit('startGameInit')			
			this.traslateMoto(this.pointer1.x)
			this.superBullet(this.controFire)
		}else{
			this.traslateMoto(this.pointer1.x)
			this.moto.body.setVelocityX(0)			
		}
		
		if (this.pointer2.isDown){		
			this.traslateMoto(this.pointer2.x)
			this.superBullet(this.controFire)
		}


		// for(let i = 0; i < this.pawnGroup.getChildren().length; i++){
		// 	let pawn = this.pawnGroup.getChildren()[i]
		// 	log('this.pawnGroup.getChildren().length', this.pawnGroup.getChildren().length)
		// 	if(pawn.getBounds().bottom >= this.height ){
		// 		log('ESTOY', 'FUERA PERO NO PUERTO')
		// 		pawn.destroy()	
		// 		this.pawnGroup.addPawn()									
		// 	}
		// }

	}

	superBullet(time){
		if( this.adwarBullet > 0){
			if (time % 6 == 0){
				this.fire()
				this.adwarBullet--
			}	
		}else{			
			if (time % 20 == 0){
				this.bulletGroup.clearTexture()
				this.fire()
			}	
		}		
	}



	drawBossLife(resistance, life){
		this.rectloader.clear();
		this.rectloader.fillStyle(0xFEF200, 1);
		this.rectloader.fillRoundedRect((this.width/2)-53, (this.height+100)/4, 120, 9, 3);
		this.rectloader.setAlpha(0.8)

		
		let porcentage = Math.round((resistance))
		let progress = (porcentage / life)*100
		this.progressBar.clear();
		this.progressBar.fillStyle(0x00070D, 1);
		this.progressBar.fillRect((this.width/2) -50, (this.height+100)/4 + 3, progress, 3);
	}

	clearBossLife(){
		this.rectloader.clear();
		this.progressBar.clear();
	}



}


export default Moto