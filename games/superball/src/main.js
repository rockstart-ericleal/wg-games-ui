
import { _$, log, metadata } from "../../src/utils.js"
import gameOptions from './config.js';
import UI from './UI.js';
import Superball from './superball.js';
import Bootloader from './bootloader.js'
import StartGame from './start-game.js'

import Phaser from 'phaser'

window.onload = () => {
	let config = {


		title:'WiFi Gratis',
		url: 'https://wifigratis.mx',
		version:'0.0.1',
		banner: {
			hidePhaser: true,
			background:['#001957']
		},

		type: Phaser.AUTO,
		backgroundColor: 0x111B24,
		scale: {
			mode: Phaser.Scale.FIT,
			autoCenter: Phaser.Scale.CENTER_BOTH,
			parent: "render-game",
			width: ((window.innerWidth > 500) ? 500 : window.innerWidth),
			height: window.innerHeight,
			// zoom: 1 / window.devicePixelRatio
		},
		physics: {
			default: "arcade",
			arcade:{
				// gravity: {
				// 	y:800
				// },
				// debug: true,
			}
		},
		scene: [Bootloader, UI, Superball, StartGame]
		//scene: [StartGame]

	}

	const game = new Phaser.Game(config);
	window.focus();


	window.onerror = (errorMsg, url, lineNumber) => {
		console.log(errorMsg,url,lineNumber);
	    let el = document.querySelector("body");
		el.innerHTML = '<h5>'+errorMsg+'</h5><a href="'+url+'">'+url+'</a><h5>'+lineNumber+'</h5>';
	};
}