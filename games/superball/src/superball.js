import Phaser from 'phaser'

import gameOptions from './config.js'
import { _$, log, metadata } from "../../src/utils.js"
import RewardGroup from './game-objects/reward-group.js'

var firstStartGame = true
var LEVEL = gameOptions.level
var LIFES = 3
var TIME_HEART = true
var TIME_SKULL = true

var CONTROL_BALL = false

class Superball extends Phaser.Scene{
	constructor(){
		super("Superball")
	}

	init(){
		this.events.on('pause', function(){ 
			this.scene.setVisible(false, 'Superball');
		}, this);
		this.events.on('resume', function(){ 
			this.scene.setVisible(true, 'Superball');
		}, this);
	}

	preload(){



		this.load.path = './games/superball/assets/'
		

		
		//this.load.multiatlas('UIG','./images/data-images.json')	
		// this.load.image('ball', ['images/ball.png'])
		// this.load.image('commet', ['images/commet.png'])
		// this.load.image('ground', ['images/ground.png'])
	}

	create(){
		this.cameras.main.setRoundPixels(true);
		this.soundFireActive = false
		let rnd = new Phaser.Math.RandomDataGenerator((Date.now() * Math.random()).toString())
		this.sound.stopAll()

		//Cameras
		this.camera = this.cameras.main

		//Sounds
		this.bounceSound = this.sound.add('fx-bounce')
		this.bounceSound.volume = 0.7

		this.winnerSound = this.sound.add('winner-1')
		this.winnerSound.volume = 0.5

		this.shakeSound = this.sound.add('shake')
		this.shakeSound.volume = 0.7

		this.fireSound = this.sound.add('end-game')

		this.platformGroup = this.physics.add.group()
		this.fireGroup = this.add.group()

		this.resetPositionY = this.game.config.height * gameOptions.groundPosition - gameOptions.ballHeight
		
		this.supportBall = this.physics.add.image(this.game.config.width * gameOptions.ballPosition, this.resetPositionY ,"UI", "ball")
		this.supportBall.setAlpha(0)

		this.ball = this.physics.add.image(this.game.config.width * gameOptions.ballPosition, this.resetPositionY ,"UI", "ball")
		this.ball.body.gravity.y = gameOptions.ballGravity
		this.ball.setVelocityY(-300)
		this.ball.setBounce(1)
		this.ball.setScale(0.7)
		this.ball.body.checkCollision.down = true
        this.ball.body.checkCollision.up = false
        this.ball.body.checkCollision.left = false
        this.ball.body.checkCollision.right = false
        this.ball.setSize(30, 50, true)
        this.ball.body.setCircle(18)
        this.ball.body.setOffset(18, 15)
//        this.ball.setDepth(-1);


        this.rewardGroup = new RewardGroup({
			physicsWorld: this.physics.world,
			scene: this,
			width:this.width,
			height:this.height,
			level: LEVEL
		})


        //First Platform 
        let platformX = this.ball.x

        for ( let i = 0; i < 5; i++ ){
        	let platform = this.platformGroup.create(0, 0, "ground-white" )
        	platform.setImmovable(true)
        	platform.body.setSize(platform.width-20, 50)
        	platform.body.setOffset(10, 10)
        	platform.setScale(0.5)

        	this.placePlatform(i, platform, platformX)
        	platformX += Phaser.Math.Between(gameOptions.platformDistanceRange[0], gameOptions.platformDistanceRange[1])
        	
			this.rewardGroup.addRewardStock(1, platform.x, platform.y - 32)
        }


        
		

        this.input.on("pointerdown", this.movePlatforms, this)
        this.input.on("pointerup", this.stopPlatforms, this)

        let particles = this.add.particles('UI', 'commet')
        let emitter = particles.createEmitter({
	        speed: 50,
	        scale: { start: .4, end: 0},
	        blendMode: 'ADD',
	        gravityY: 350,
	        lifespan: 500
		})
	    emitter.startFollow(this.ball)

	    let exploFire = this.add.particles('UI','commet')
	    this.explotion = exploFire.createEmitter({
	    	active:false, 
	    	yoyo: false, 
	    	quantity: 1,
	    	scale: { start: .8, end: 0},
	    })
	    this.explotion.setLifespan(600)
	    this.explotion.setSpeed(200)

	    if (firstStartGame){
	    	this.scene.pause('Superball')
	    	this.scene.setVisible(false, 'Superball')
	    	firstStartGame = false
	    }

	    let camera = this.camera
		let bounceSound = this.bounceSound

		this.supportBall




		//this.physics.add.overlap	
		let tempSound = this.winnerSound
		let scene = this

		this.physics.add.collider(this.platformGroup, this.ball, function(platform, ball){
        	if(ball.fallingDown){
        		ball.body.gravity.y = 1000
        	}
        	camera.shake(50, 0.01)
        	bounceSound.play()
        	scene.ball.body.velocity.y = -1200
        	        	
        })


		this.time.addEvent({
			delay:9000,
			callback: ()=>{ 
				TIME_HEART = true
			},		    
		    callbackScope: this,
		    repeat: -1
		})


		this.time.addEvent({
			delay:4000,
			callback: ()=>{ 
				TIME_SKULL = true
			},		    
		    callbackScope: this,
		    repeat: -1
		})

		//

		this.time.addEvent({
			delay:5000,
			callback: ()=>{ 
				CONTROL_BALL = true
			},		    
		    callbackScope: this,
		    repeat: -1
		})

		this.cameras.main.on(Phaser.Cameras.Scene2D.Events.SHAKE_COMPLETE, ()=>{
			this.shakeSound.stop()
			scene.shakeSound.setLoop(false)
		})
		
		let rect = new Phaser.Geom.Rectangle(0, 0, this.scale.width, this.scale.height);
		this.physics.add.overlap(this.rewardGroup, this.ball, function(ball, reward){
        	
			
        	if (!reward.catch){

        		
        		        		
        		reward.catch = true        		
        		tempSound.play()
        		log('this.resetPositionY', scene.resetPositionY)

        		if(reward.texture.key == 'reward-0'){
        			scene.cameras.main.shake(2400, 0.015)
        			scene.shakeSound.setLoop(true)
        			scene.shakeSound.play()
        		}


        		if(reward.texture.key != 'reward-8'){        			
        			scene.updateScore(5)	
        		}else{
        			scene.registry.events.emit('reward-life')
        			LIFES++
        		}

        		let particles = scene.add.particles(reward.texture.key);				
			    particles.createEmitter({
			     
			        x: reward.x, y: reward.y,
			        lifespan: 4000,
			        speed: { min: 60, max: 80 },
			        scale: { start: 0.12, end: 0 },
			        gravity: 0,			        
			        bounce: 0.9,
			        bounds: rect,
			        // blendMode: 'ADD',
			        frequency: 8,
			        maxParticles: 10,
			    });

			    reward.y = scene.scale.height + 32
        	}
        	
        })

	    this.setEventsNotify()
	}




	setEventsNotify(){
		this.registry.events.on('startNextGame', ()=>{
			this.scene.start('Superball')
		})

		this.registry.events.on('upgradeLevel', ()=>{
			this.ball.body.gravity.y += 100
		})
	}

	placePlatform(index, platform, posX){
		platform.x = posX
		//Variante de altura de plataformas.
		platform.y = this.game.config.height * gameOptions.groundPosition + Phaser.Math.Between(gameOptions.platformHeightRange[0], gameOptions.platformHeightRange[1])

		//Permitir caer a la plataforma				
		if(index == 0){
			platform.fallingDown = false
		}else{
			platform.fallingDown = true
		}

		platform.displayWidth = Phaser.Math.Between(gameOptions.platformLengthRange[0], gameOptions.platformLengthRange[1])

		platform.body.gravity.y = 0
		platform.body.velocity.y = 0

		
	}

	movePlatforms(event){
		if(event.y >= 200){
			this.platformGroup.setVelocityX(-gameOptions.platformSpeed)	
			this.rewardGroup.setVelocityX(-gameOptions.platformSpeed)
		}
	}

	stopPlatforms(){
		this.platformGroup.setVelocityX(0)
		this.rewardGroup.setVelocityX(0)
	}

	update(){

        this.platformGroup.getChildren().forEach(function(platform){
        	if (platform.getBounds().right < 0 ){
     		
        		this.updateScore(1)
        		this.placePlatform(1,platform, this.getRightmostPlatform() + Phaser.Math.Between(gameOptions.platformDistanceRange[0], gameOptions.platformDistanceRange[1]) )
        		
        		this.rewardGroup.getChildren().forEach(function(reward){
		        	if (reward.x < 0 ){
		        		reward.x = platform.x
		        		reward.y = platform.y - 32

		        		let texture = 1
		        		if(LIFES < 3 && TIME_HEART){
		        			texture = 8
		        			TIME_HEART = false
		        			log('texture', texture)
		        		}else{
		        			texture = Phaser.Math.Between(1,7)
		        		}
		        		
		        		if(TIME_SKULL){
		        			texture = Phaser.Math.Between(0,1)
		        			TIME_SKULL = false
		        		}

		        		reward.catch = false
		        		reward.setTexture('reward-'+texture)
		        		reward.setScale(0.3)

		        		let posibleVisible = Phaser.Math.Between(0,1)
						if(posibleVisible){
							reward.y = this.scale.height + 32
						}		        	
		        		return 0
		        	}
		        }, this)
        		return 0
        	}
        }, this)

        if(this.ball.y > this.game.config.height){         
           	
          	if (this.soundFireActive == false){
          		this.fireSound.play()
          			            
	            this.explotion.setBlendMode(Phaser.BlendModes.ADD)
	            this.explotion.setPosition(this.ball.x, this.game.config.height)
	            this.explotion.active = true

	            
	   			let scene = this   		
	   			this.registry.events.emit('gameOverNotify')
	   			LIFES--
	            this.time.delayedCall(1600, function() {
	            	scene.registry.events.emit('gameScoreNotify', 0)
	            	scene.registry.events.emit('reloadScene')
				    scene.scene.start('Superball')				    
				})
          	}
          	this.soundFireActive = true    
        }
	}

	getRightmostPlatform(){
		let rightmostPlatform = 0
		this.platformGroup.getChildren().forEach(function(platform){
			rightmostPlatform = Math.max(rightmostPlatform, platform.x)
		}, this)
		return rightmostPlatform
	}

	updateScore(currentScore){
		this.registry.events.emit('gameScoreNotify', currentScore)
		this.registry.events.emit('updateLevelControl')
	}
}


export default Superball