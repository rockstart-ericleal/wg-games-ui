import Phaser from 'phaser'
//var Phaser = require('phasers')

import gameOptions from './config.js'
import { _$, log, metadata, ratioDisplay } from "../../src/utils.js"

var isLaunch = false

class Bootloader extends Phaser.Scene{
	constructor(){
		super("Bootloader")
	}

	init(){

	}

	preload(){
		this.cameras.main.setRoundPixels(true);
			//Load Flyer
		this.load.image('flyer', gameOptions.urlAds)
	
		this.load.path = gameOptions.assetsPath()

		this.load.multiatlas('UIP','./images/data-images.json')
		this.load.image('logo','./images/logo-wifi.png')
		//Load Sounds
		this.load.audio('explosion-shine','./sounds/explosion-shine.mp3')
		this.load.audio('coins','./sounds/coins.mp3')
		this.load.audio('next-level-up','./sounds/next-level-up.mp3')

		let ratio = ratioDisplay()
		log('Ratio', ratio)
		this.load.image('start-game-bg-top', `./images/start-game-bg-top@${ratio}.png`)
		this.load.image('start-game-bg-bottom', `./images/start-game-bg-bottom@${ratio}.png`)
		this.load.image('play-yellow', `./images/play-yellow@${ratio}.png`)
		this.load.image('super-ball-header', `./images/super-ball-header@${ratio}.png`)
		this.load.image('super-ball-bottom', `./images/super-ball-bottom@${ratio}.png`)
		this.load.image('title-connect', `./images/title-connect@${ratio}.png`)

		this.load.multiatlas('FLYER','/images/data-images.json')	
		this.load.image('top-fade-black', './images/top-fade-black.png')
		this.load.image('reward-8', './images/reward-8.png')
		
		this.load.multiatlas('UI','./images/data-images.json')		
		
		this.load.image('top-fade-black', './images/top-fade-black.png')

		//Load Sounds
		this.load.audio('explosion-shine','./sounds/explosion-shine.mp3')
		this.load.audio('coins','./sounds/coins.mp3')
		this.load.audio('next-level-up','./sounds/next-level-up.mp3')

		this.load.image('reward-1', './images/reward-1.png')
		this.load.image('reward-2', './images/reward-2.png')
		this.load.image('reward-3', './images/reward-3.png')
		this.load.image('reward-4', './images/reward-4.png')
		this.load.image('reward-5', './images/reward-5.png')
		this.load.image('reward-6', './images/reward-6.png')
		this.load.image('reward-7', './images/reward-7.png')
		this.load.image('reward-8', './images/reward-8.png')
		this.load.image('reward-0', './images/reward-0.png')


		//Sounds
		this.load.audio('fx-bounce', ['./sounds/bounce-ball-1.mp3'])
		this.load.audio('fx-sound-bg', ['./sounds/sound-bg.mp3'])
		this.load.audio('end-game', ['./sounds/incident_game.mp3'])
		this.load.audio('winner-1', ['./sounds/winner_1.mp3'])
		this.load.audio('shake', ['./sounds/shake.mp3'])
		//

		this.load.image('ground-white', './images/ground-.png')
		
		let rectloader = this.add.graphics();
		rectloader.fillStyle(0xffffff, 1);
		rectloader.fillRoundedRect((this.scale.width/2)-63, this.scale.height/2, 126, 9, 3);

		let textPorcentage = this.make.text({
			x: this.scale.width/2,
            y: this.scale.height/2 - 32,
            text: '0%',
            style: {
                fontSize: '32px', align: 'left', fontFamily: "'Geogrotesque Medium'"
            }
        })

        textPorcentage.setOrigin(0.5)

		let progressBar = this.add.graphics();

		this.load.on('progress', (value) => {
			let porcentage = Math.round((value * 100))
			let progress = (porcentage * 120)/100
			progressBar.clear();
			progressBar.fillStyle(0x00070D, 1);
			progressBar.fillRect((this.scale.width/2) -60, this.scale.height/2 + 3, progress, 3);			
			
			this.add.tween({
				targets:textPorcentage,
				duration:200,
				ease:'Bounce',
				scale:{ from: 1.2, to: 1},
				onComplete: () =>{
					textPorcentage.text = `${porcentage}%`
				}
			})


			if (porcentage >= 100 ){
				
				this.add.tween({
					targets: [rectloader, progressBar, textPorcentage],
					alpha:{from: 1, to: 0},
					duration: 500,
					repeat:0,
					delay:1000,
					onConplete: () => {
						this.time.delayedCall(600, ()=>{
							this.loadLogo()
						})
					}
				})
				log('porcentage', porcentage)


			}

		})

		
	}

		loadLogo(){
		this.logo = this.add.image(this.scale.width/2, this.scale.height/2, 'logo')
		this.logo.setAlpha(0)
		this.logo.setScale(0.6)

		if(!isLaunch){
			isLaunch = true
			this.time.delayedCall(300, ()=>{

				this.add.tween({
				targets: [this.logo],
				alpha:{from: 0, to: 1},
				duration: 800,
				repeat:0,
				onComplete: () => {

					this.add.tween({

						targets: [this.logo],
						alpha:{from: 1, to: 0},
						duration: 400,
						repeat:0,
						delay:1000,
						onComplete: () =>{
							this.scene.start('StartGame')
							this.scene.start('UI')
						}

					})

										
				}
			})

			})

		}	
		


	}



	create(){

		

		// graphics.x = this.camera.width/2
		// graphics.x = this.camera.height/2
		
	}

}

export default Bootloader