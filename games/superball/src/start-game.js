import Phaser from 'phaser'
import gameOptions from './config.js'

import { _$, log, metadata, getScaleFitFlyer, ratioDisplay, connetToWiFi } from "../../src/utils.js"

class StartGame extends Phaser.Scene {
	constructor(){
		super({key:'StartGame'})

	}
	init(){
		log('Init Start Game')
	}

	preload(){
		
		// this.load.path = './games/superball/assets/'
		// let ratio = ratioDisplay()
		// log('Ratio', ratio)
		// this.load.image('start-game-bg-top', `./images/start-game-bg-top@${ratio}.png`)
		// this.load.image('start-game-bg-bottom', `./images/start-game-bg-bottom@${ratio}.png`)
		// this.load.image('play-yellow', `./images/play-yellow@${ratio}.png`)
		// this.load.image('super-ball-header', `./images/super-ball-header@${ratio}.png`)
		// this.load.image('super-ball-bottom', `./images/super-ball-bottom@${ratio}.png`)
		// this.load.image('title-connect', `./images/title-connect@${ratio}.png`)
	}

	create(){
		this.cameras.main.setRoundPixels(true);
		let { width, height } = this.sys.game.canvas;
		this.height = height
		this.width = width
		let cameraMain = this.cameras.main

		this.backgroundTop = this.add.image(width/2, height/2, 'start-game-bg-top')
		this.backgroundTop.setScale(getScaleFitFlyer(cameraMain, this.backgroundTop).scale).setScrollFactor(0)
		this.backgroundTop.setOrigin(0.5, 1)

		this.backgroundBottom = this.add.image(width/2, height/2, 'start-game-bg-bottom')
		this.backgroundBottom.setScale(getScaleFitFlyer(cameraMain, this.backgroundBottom).scale).setScrollFactor(0)
		this.backgroundBottom.setOrigin(0.5, 0)

		
		this.playYellow = this.add.image(width/2, height/2,'play-yellow')
		this.playYellow.setScale(0.3)
		this.playYellow.setInteractive()

		this.playYellow.on(Phaser.Input.Events.POINTER_UP, ()=>{
			this.hideControlsStartGame()
			this.openSceneGame()
			this.emitRegistry('startGame')
			
		})

		this.headerTitle = this.add.image(width/2, height/2 - (this.playYellow.displayWidth + 18),'super-ball-header')
		this.headerTitle.setScale(0.3)

		this.bottomTitle = this.add.image(width/2, height/2 + (this.playYellow.displayWidth + 18),'super-ball-bottom')
		this.bottomTitle.setScale(0.3)

		this.titleConnect = this.add.image(width/2, height/2,'title-connect')
		this.titleConnect.setScale(0)
		

		this.add.tween({
			targets:[this.playYellow, this.bottomTitle],
			scale:{ from: 0.35, to: 0.3},
			duration: 500,
			yoyo: true,
			repeat: -1,
			ease:'Linear'
		})





		this.eventsRegistry()		

	}


	toConnectWifi(){
		this.add.tween({
			targets:[this.backgroundTop],
			y:{ from: 0, to: this.height/2},			
			duration: 800,			
			repeat: 0,
			ease:'Expo.easeOut',
			onComplete: () => {
				this.showTitleConnet()
				let data = {
					userId: gameOptions.userId,
					anuncioId: gameOptions.anuncioId,
					chap: gameOptions.chap,
					chapChallenge: gameOptions.chapChallenge
				}
				connetToWiFi(data)
			}
		})

		this.add.tween({
			targets:[this.backgroundBottom],
			y:{ from: this.height, to: this.height/2 },
			duration: 800,			
			repeat: 0,
			ease:'Expo.easeOut'
		})
	}

	reloadScene(){
		this.add.tween({
			targets:[this.backgroundTop],
			y:{ from: 0, to: this.height/2},			
			duration: 400,			
			repeat: 0,
			ease:'Expo.easeOut',
			onComplete: () => {
				this.add.tween({
					targets:[this.backgroundTop],
					y:{ from: this.backgroundTop.y, to: 0},			
					duration: 400,			
					repeat: 0,
					ease:'Expo.easeOut'
				})
			}
		})

		this.add.tween({
			targets:[this.backgroundBottom],
			y:{ from: this.height, to: this.height/2 },
			duration: 400,			
			repeat: 0,
			ease:'Expo.easeOut',
			onComplete: () => {
				this.add.tween({
					targets:[this.backgroundBottom],
					y:{ from: this.backgroundBottom.y, to: this.height},
					duration: 400,			
					repeat: 0,
					ease:'Expo.easeOut'
				})
			}
		})
	}

	showTitleConnet(){
		this.add.tween({
			targets:[this.titleConnect],
			scale:{ from: 0.32, to: 0.3},
			duration: 800,
			yoyo: true,
			repeat: -1,
			ease:'Linear'
		})
	}

	openSceneGame(){
		this.add.tween({
			targets:[this.backgroundTop],
			y:{ from: this.backgroundTop.y, to: 0},			
			duration: 800,			
			repeat: 0,
			ease:'Expo.easeOut'
		})

		this.add.tween({
			targets:[this.backgroundBottom],
			y:{ from: this.backgroundBottom.y, to: this.height},
			duration: 800,			
			repeat: 0,
			ease:'Expo.easeOut'
		})
	}

	closeSceneGame(){
		this.add.tween({
			targets:[this.backgroundTop],
			y:{ from: 0, to: this.height/2},			
			duration: 800,			
			repeat: 0,
			ease:'Expo.easeOut'
		})

		this.add.tween({
			targets:[this.backgroundBottom],
			y:{ from: this.height, to: this.height/2 },
			duration: 800,			
			repeat: 0,
			ease:'Expo.easeOut'
		})
	}

	hideControlsStartGame(){
		this.add.tween({
			targets:[this.playYellow, this.headerTitle, this.bottomTitle],
			scale:{ from: 0.32, to: 0},
			alpha:{ from: 1, to: 0},
			duration: 200,			
			repeat: 0,
			ease:'Linear'
		})
	}

	showControlsStartGame(){
		this.add.tween({
			targets:[this.playYellow, this.headerTitle, this.bottomTitle],
			scale:{ from: 0, to: 0.3},
			alpha:{ from: 0, to: 1},
			duration: 200,			
			repeat: 0,
			ease:'Linear'
		})
	}

	emitRegistry(event){
		this.registry.events.emit(event)
	}

	eventsRegistry(){
		this.registry.events.on('openSceneGame', ()=> { this.openSceneGame() })
		this.registry.events.on('closeSceneGame', ()=> { this.closeSceneGame() })
		this.registry.events.on('hideControlsStartGame', ()=> { this.hideControlsStartGame() })
		this.registry.events.on('showControlsStartGame', ()=> { this.showControlsStartGame() })
		this.registry.events.on('showTitleConnet', ()=> { this.showTitleConnet() })
		this.registry.events.on('reloadScene', ()=> { this.reloadScene() })
		this.registry.events.on('toConnectWifi', ()=> { this.toConnectWifi() })
		

		
	}

}

export default StartGame