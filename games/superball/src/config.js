import { _$, log, metadata } from "../../src/utils.js"

const gameOptions = {
	groundPosition: (6 / 8),
	ballHeight: 360,
	ballGravity: 2000,
	// ballGravity: 0,
	ballPosition: (1/4),
	platformSpeed: 650,
	platformDistanceRange: [160, 300],
	platformHeightRange: [0, 0],
	platformLengthRange: [56, 120],
	platformLengthPercent: 50,
	fallingPlatformPercent: 2,
	localStorageName: "score-wg",
	amount_stars: 16,
	urlAds: metadata()["url-ad"],
	chap: metadata()["chap"],
	chap: metadata()["chap-id"],
	chapChallenge: metadata()["chap-challenge"],
	dst: metadata()["dst"],
	domainAssets: metadata()["domain-assets"],
	miScore: metadata()["mi-score"],
	userName: metadata()["user-name"].toUpperCase(),
	
	gameId: parseInt(metadata()['game-id']),
	typeFlyer: metadata()['type-flyer'],

	token:metadata()["token"],
	bestScore: parseInt(metadata()["best-score"]),
	anuncioId: parseInt(metadata()['anuncio-id']),	
	level: parseInt(metadata()["level"]),
	gameName: metadata()["game-name"],
	userId: parseInt(metadata()['user-id']),
	anuncioGameId: parseInt(metadata()['anuncio-game-id']),

	
	production: metadata()['production'],
	assetsPath: function(){
		if( this.production === 'true'){
			return this.domainAssets + '/games/superball/assets/'
		}else{
			return './games/superball/assets/'
		}
	}


};

export default gameOptions;