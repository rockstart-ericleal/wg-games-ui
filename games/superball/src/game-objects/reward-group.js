import Phaser from 'phaser'

import gameOptions from '../config.js'
import { _$, log, metadata } from "../../../src/utils.js"


class RewardGroup extends Phaser.Physics.Arcade.Group{
	constructor(config){
		super(config.physicsWorld, config.scene)		
		this.scene = config.scene
		this.texure = 'reward-1'
		this.level = config.level || 1			
	}

	addReward(x, y){
	
	}

	addRewardStock(total, x, y){
		console.log("REW",x,y)
		let reward = this.create(x, y, this.texure)	
		reward.setScale(.3)
		reward.body.gravity.y = 0
		reward.body.velocity.y = 0
		reward.catch = false

		let texture = Phaser.Math.Between(1,7)		
		reward.setTexture('reward-'+texture)

		let posibleVisible = Phaser.Math.Between(0,1)
		if(posibleVisible){
			reward.y = this.scene.scale.height + 32
		}
	}

	allDestroy(){		
		this.children.iterate( (reward) =>{
			reward.destroy()
		})
	}


	distributeReward(){
		
	}

}

export default RewardGroup
