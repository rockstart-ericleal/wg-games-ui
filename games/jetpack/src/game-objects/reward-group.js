import gameOptions from '../config.js'
import { _$, log, metadata } from "../../../src/utils.js"


class RewardGroup extends Phaser.Physics.Arcade.Group{
	constructor(config){
		super(config.physicsWorld, config.scene)		
		this.scene = config.scene
		this.texure = 'reward-8'
		this.level = config.level || 1			
	}

	addReward(x, y){
	
	}

	addRewardStock(total, x, y){
		console.log("REW",x,y)
		let reward = this.create(x, y, this.texure)	
		reward.setScale(.32)
		reward.body.gravity.y = 0
		reward.body.velocity.y = 0
		reward.catch = false

		this.scene.add.tween({
			targets: reward,			
			props:{
				scale:{
					value:{ from: .38, to: .32},
					repeat:-1,
					duration:1000,
					ease: 'Bounce',
				},
				x:{
					duration: 20000,
					value: {from: reward.x, to: -100},
					ease: 'Cubic.easeInOut',
					repeat:-1,
				}
			}
		})
	}

	allDestroy(){		
		this.children.iterate( (reward) =>{
			reward.destroy()
		})
	}


	distributeReward(){
		
	}

	update(lifes){
		let ry = Phaser.Math.Between(100, this.scene.scale.height -100) 
		let rx = Phaser.Math.Between(2000, 3000) 
		let counter = 0
		this.children.iterate( (reward) =>{
			if ( reward.getBounds().right < 0 ){
				reward.y = ry
				reward.x = rx
			}
			counter++
		})

		if(counter == 0){
			
			if(lifes < gameOptions.initLife){
				this.addRewardStock(1, rx, ry)
			}
			
		}
	}

}

export default RewardGroup
