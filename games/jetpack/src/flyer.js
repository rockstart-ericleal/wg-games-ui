import gameOptions from './config.js'
import { _$, log, metadata, getScaleFitFlyer, shadowDistance, getAutoHeight } from "../../src/utils.js"


class Flyer extends Phaser.Scene{
	constructor(){
		super("Flyer");
	}

	init(){
		log('Init Flyer')		
	}

	preload(){

		
	}

	create(){
		this.cameras.main.setRoundPixels(true);
		let cameraMain = this.cameras.main

		let flyer = this.add.image(cameraMain.width / 2, cameraMain.height / 2, 'flyer')
		let scaleFitFlyer = getScaleFitFlyer(cameraMain, flyer)		
		flyer.setScale(scaleFitFlyer.scale).setScrollFactor(0)
		flyer.setOrigin(0.5)
		flyer.setAlpha(0.7)

		this.add.tween({
			targets:[flyer],
			duration: 3000,
			yoyo:true,
			y: cameraMain.height / 2 + 21,
			repeat:-1
		})

		this.layerTopFadeBlackTop = this.add.image(this.scale.width/2, this.scale.height/2, 'top-fade-black')
		let distanceShadow = shadowDistance(this.scale.height, flyer.displayHeight, this.layerTopFadeBlackTop)
		this.layerTopFadeBlackTop.y = distanceShadow.top
		this.layerTopFadeBlackTop.setDisplaySize(this.scale.width, 50)
		this.layerTopFadeBlackTop.setOrigin(0.5, 0)


		let layerTopFadeBlackBottom = this.add.image(this.scale.width/2, this.scale.height/2, 'top-fade-black')
		layerTopFadeBlackBottom.setDisplaySize(this.scale.width, getAutoHeight(this.scale.width, layerTopFadeBlackBottom.width, layerTopFadeBlackBottom.displayHeight))
		layerTopFadeBlackBottom.y = distanceShadow.bottom + 32
		layerTopFadeBlackBottom.setFlipY(-1)


		this.onRegistryEvents()
		this.scene.launch('UI')

	}

	onRegistryEvents(){
		this.registry.events.on('loadTextScore', (positionY) => {
			this.layerTopFadeBlackTop.setDisplaySize(this.scale.width, positionY)			
		})
	}

	update(){
		
	}
}


export default Flyer