import gameOptions from './config.js'
import { _$, log, metadata } from "../../src/utils.js"
import RewardGroup from './game-objects/reward-group.js'

var floatX = 0
var floatY = 0
var control = 0
var LEVEL = 1
var LIFES = gameOptions.initLife


class Jetpack extends Phaser.Scene{
	constructor(){
		super("Jetpack");
	}

	init(){
		log('Init Jetpack')
		this.events.on('pause', function(){ 
			this.scene.setVisible(false, 'Jetpack');
		}, this);
		this.events.on('resume', function(){ 
			this.scene.setVisible(true, 'Jetpack');
		}, this);
	}

	preload(){


		//this.load.path = './games/jetpack/assets/'

		
		
	}

	create(){


		this.winnerSound = this.sound.add('winner')
		this.gameover = this.sound.add('error')

		//Camera
		this.camera = this.cameras.main;

		let particles = this.add.particles('spark');
        let emitter = particles.createEmitter({
	        alpha: { start: 1, end: 0 },
	        scale: { start: 0.05, end: 1 },
	        //tint: { start: 0xff945e, end: 0xff945e },
	        speed: 20,
	        accelerationX: -500,
	        angle: { min: -85, max: -95 },
	        rotate: { min: -180, max: 180 },
	        lifespan: { min: 1000, max: 1100 },
	        blendMode: 'ADD',
	        frequency: 30,
	        //maxParticles: 10,
	        x: -25
	    });


        this.premioGroup = this.physics.add.group();
		this.extraReward = this.add.image(200, 400,'extra-reward');
        this.extraReward.setScale(.4);
        this.extraReward.setAngle(45);

        this.fireTweens = this.add.tween({
			targets:[this.extraReward],
			ease:'Sine.easeInOut',
			// y:(this.game.config.height) -30,
			angle: -45,
			repeat: -1,
			yoyo: true,
			duration:3000

		});

        this.premioGroup.add(this.extraReward);


        this.anims.create({
			key: 'trap',		    
		    frames: this.anims.generateFrameNumbers('pipe', { start: 0, end: 32 }),
		    frameRate: 45,
		    repeat: -1
		});

        this.rewardGroup = new RewardGroup({
			physicsWorld: this.physics.world,
			scene: this,
			width:this.width,
			height:this.height,
			level: LEVEL
		})

		this.rewardGroup.update(LIFES)

        this.pipeGroup = this.physics.add.group();
		this.pipePool = [];

		for ( let i = 0; i < 4; i++){			
			this.pipePool.push(this.pipeGroup.create(0, 0, 'pipe'));
			this.pipePool.push(this.pipeGroup.create(0, 0, 'pipe'));
			//this.pipeGroup.getChildren()[i].anims.play('trap');

			this.placePipes(false);
		}


		this.pipeGroup.setVelocityX(0);



		this.pipeGroup.setVelocityX(0);
		this.bird = this.physics.add.sprite(this.camera.width/4, this.camera.height / 2, 'wifito');
		this.bird.setScale(.75)
		this.bird.body.gravity.y = gameOptions.birdGravity;
		this.bird.body.setCircle(28);
		//this.bird.body.setSize(70, 50);
		this.bird.body.setOffset(28, 15);

		


		let style = { fontFamily: "'Geogrotesque Medium'", fill: "#fff", boundsAlignH: "center", boundsAlignV: "middle", fontSize: '74px' };
	    this.winnerPointsText = this.add.text(0, 0, '+5', style);
	    this.winnerPointsText.setAlpha(0)

		emitter.startFollow(this.bird);
		this.premioWiner();

		this.setRegidtryEvents()
		
		this.emitGameOver = true

		this.physics.add.collider(this.bird, this.pipeGroup, function(){
            this.die();
        }, null, this);
        
		//

		this.physics.add.overlap(this.bird, this.rewardGroup, function(bird, reward){
            if(!reward.catch){
            	reward.catch = true
            	this.winnerSound.play()
            	this.rewardGroup.allDestroy()
            	LIFES++
            	this.registry.events.emit('addLifeDisplay')
            	this.rewardGroup.update(LIFES)
            }
        }, null, this);

		if(control > 0){
			this.input.on('pointerdown', this.flap, this);
		}
	}

	setRegidtryEvents(){
		this.registry.events.on('geTextPoint', (x, y) => {
			floatX = x
			floatY = y
		})

		this.registry.events.on('startNextGame', () => {
			this.scene.start('Jetpack')
		})	

		this.registry.events.on('startCapturePointer', () => {
			this.input.on('pointerdown', this.flap, this);
		})	
		
		// this.registry.events.on('startGame', () => {
		// 	this.bird.body.gravity.y = 800;
		// })
	}

	premioWiner(){
		let rangePosition = this.game.scale.height/6;
		let positionRandom = Phaser.Math.Between(rangePosition, this.game.scale.height-rangePosition);
	    this.premioGroup.setXY(this.game.scale.width + positionRandom, positionRandom);
	    this.premioGroup.setVelocityX(-160);  
	    this.sumPoints = true; 
	    this.premioGroup.getChildren()[0].setScale(0.3);
	}

	flap(event){
		control++
		log('Event', event.camera.cameraManager.systems.config)

        this.bird.body.velocity.y = -gameOptions.birdFlapPower;
        this.bird.body.gravity.y = 800;
        this.pipeGroup.setVelocityX(-gameOptions.birdSpeed);
        this.registry.events.emit('startGameInit')
    }

	placePipes(addScore){

		let texturePipe = 'pipe';
		let rightmost = this.getRightmostPipe();
		let pipeHoleHeight = Phaser.Math.Between(gameOptions.pipeHole[0], gameOptions.pipeHole[1]);
		let pipeHolePosition = Phaser.Math.Between(gameOptions.minPipeHeight + pipeHoleHeight / 2, this.camera.height - gameOptions.minPipeHeight - pipeHoleHeight / 2);
		

		this.pipePool[0].x = rightmost + this.pipePool[0].getBounds().width + Phaser.Math.Between(gameOptions.pipeDistance[0], gameOptions.pipeDistance[1]);
		this.pipePool[0].y = pipeHolePosition - pipeHoleHeight / 2;
		this.pipePool[0].setOrigin(0, 1);
		this.pipePool[0].setTexture(texturePipe);
		this.pipePool[0].anims.play('trap');
		this.pipePool[0].setSize(45, 1220);
		this.pipePool[0].setOffset(10, 0);
				
		this.pipePool[1].x = this.pipePool[0].x;
		this.pipePool[1].y = pipeHolePosition + pipeHoleHeight / 1;
		this.pipePool[1].setOrigin(0, 0);
		this.pipePool[1].setTexture(texturePipe);
		this.pipePool[1].anims.play('trap');
		this.pipePool[1].setSize(45, 1220);
		this.pipePool[1].setOffset(10, 6);

		this.pipePool = [];
		if(addScore){
            this.updateScore(1);
            this.registry.events.emit('updateLevelControl')
        }
	}

	getRightmostPipe(){
        let rightmostPipe = 0;
        this.pipeGroup.getChildren().forEach(function(pipe){
            rightmostPipe = Math.max(rightmostPipe, pipe.x);
        });
        return rightmostPipe;
    }

	update(){

		this.rewardGroup
		this.rewardGroup.update(LIFES)
		
		if(this.bird.y > this.camera.height || this.bird.y < 0){
            this.die();
        }

        this.pipeGroup.getChildren().forEach(function(pipe){
            if(pipe.getBounds().right < 0){
                this.pipePool.push(pipe);
                if(this.pipePool.length == 2){
                    this.placePipes(true);
                }
            }
        }, this);


        if(this.distance(this.bird.x, this.bird.y, this.premioGroup.getChildren()[0].x, this.premioGroup.getChildren()[0].y) <= 50){        	
        	if(this.sumPoints){
        		this.updateScore(5);
        		this.sumPoints = false;
        		
        		this.add.tween({
        			targets:[this.premioGroup.getChildren()[0]],
        			ease:'Sine.easeInOut',        			
        			repeat: 0,
        			x:this.bird.x,
        			y:this.bird.y,
					yoyo: false,
					duration:300,
					scale: {from:0.45, to: 0},
        		});

        		this.winnerSound.play()

        		
        		if ( floatX != 0 || floatY != 0 ){
        			this.winnerPointsText.x = this.bird.x;
	        		this.winnerPointsText.y = this.bird.y;
	        		this.winnerPointsText.setAlpha(1)
	        		this.winnerPointsText.setOrigin(0.5)

	        		this.add.tween({
	        			targets:[this.winnerPointsText],
	        			ease:'Expo.easeInOut',
	        			alpha: {from: 1, to: 0},
	        			scale:{from: 0.5, to: 0.1},
	        			repeat: 0,
	        			x:floatX,
	        			y:floatY,
						yoyo: false,
						duration:3000,					
	        		});
        		}
        		
        	}
        	
        }

	}

	getRightmostPipe(){
        let rightmostPipe = 0;
        this.pipeGroup.getChildren().forEach(function(pipe){
            rightmostPipe = Math.max(rightmostPipe, pipe.x);

        });
        return rightmostPipe;
    }
	updateScore(inc){
		control += inc	
		log('INC',inc);	
		this.registry.events.emit('gameScoreNotify', inc)	
		if(this.premioGroup.getChildren()[0].x < -100){
			this.premioWiner();			
		}
    }

	distance(x1, y1, x2, y2) {
        var dx = x1 - x2;
        var dy = y1 - y2;
        return Math.sqrt(dx * dx + dy * dy);
    }

	die(){

		if(this.emitGameOver){
			
			this.emitGameOver = false
			this.gameover.play()
			let scene = this 
			this.time.delayedCall(600, function() {

				LIFES--

				if(LIFES > 0){
					scene.registry.events.emit('reloadScene')
				}

				scene.registry.events.emit('gameScoreNotify', 0)
				scene.registry.events.emit('gameOverNotify')
			    scene.scene.start('Jetpack');

			})
		}

			        
    }
}


export default Jetpack