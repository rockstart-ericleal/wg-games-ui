
var request = new XMLHttpRequest()

const toConnect = () =>{
	window.location.href = "https://www.google.com.mx/?hl=es-419"
	// document.sendin.username.value = "flyer";
	// document.sendin.password.value = window.keywordHex;
	// document.sendin.submit();
	return false;
}

const sendOutSplash = (data) => {
	var oReq = new XMLHttpRequest();
	oReq.addEventListener("load", toConnect, false);
	oReq.open("GET", `/usuarios/usuario-kpi/${data.userId}/${data.anuncioId}/` );
	oReq.send();
}

const connetToWiFi = (data) => {	
	sendOutSplash(data)
}

const _$ = (el) => {
	return document.querySelector(el)
}

const log = (a, b) => {
	console.log(a, b)
	return 0
}

const metadata = () => {
	const metas = document.getElementsByTagName('meta')
	let metadata = Array()
	for (let i = 0; i < metas.length; i++){
		metadata[metas.item(i).getAttribute('name')] = metas.item(i).getAttribute('content')
	}
	return metadata
}

const getAutoHeight = (layerWidth, width, height) => {
	
	if (width < layerWidth){
		let widthPorcentaje = ((layerWidth - width) * 100) / layerWidth
		let addToHeight = (widthPorcentaje / 100) * height
		return height + addToHeight
	}else{
		let widthPorcentaje = ((width - layerWidth) * 100) / layerWidth
		let addToHeight = (widthPorcentaje / 100) * height
		return height + addToHeight
	}
	
}

const getScaleFitFlyer = (camera, flyer) => {

	let scaleX = camera.width / flyer.width
	let scaleY = camera.height / flyer.height

	let scale = (camera.width < camera.height) ? Math.max(scaleX, scaleX):Math.max(scaleY, scaleY)
	let info = {
		width: camera.width,
		height: camera.height,
		scale:scale
	}
	return info
}

const shadowDistance = (layerHeight, flyerHeight, gameObject) => {
	let totalDistance = layerHeight - flyerHeight
	
	return {
		top: totalDistance/2,
		bottom: (layerHeight - (totalDistance/2)) - (gameObject.displayHeight/2)
	}
}

const saveDataServer = (data) => {	
	if(data.best_score < data.current_score){
		log('Data Server:::', data)

		request.open('POST', '/anuncios/game-record/')
		//request.open('POST', "/usuarios/games-record/")
		request.setRequestHeader('Authorization', 'Bearer ' + data.token)
		request.send(JSON.stringify(data))		
	}
}

const ratioDisplay = () =>{
	let imageSize = window.devicePixelRatio * 100

	if (
		imageSize != 100 ||
		imageSize != 150 ||
		imageSize != 200 ||
		imageSize != 300 || 
		imageSize != 350
	){
		return 200
	}else{
		return imageSize
	}
}

export { _$, log, metadata, getAutoHeight, shadowDistance, getScaleFitFlyer, saveDataServer, ratioDisplay, connetToWiFi }

